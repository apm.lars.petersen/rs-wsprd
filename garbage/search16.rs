pub fn new_brute16(os: &Symbols<[f64; 2]>) -> Self {
    let xx: [(f64, f64); 162] =
        os.0.map(|x| ((x[1] - x[0]).max(0.0), (x[0] - x[1]).max(0.0)));
    let diff = xx.map(|x| (x.0 - x.1));
    Self::search16(&diff)
}

pub fn new_brute61(os: &Symbols<[f64; 2]>) -> Self {
    let xx: [(f64, f64); 162] =
        os.0.map(|x| ((x[1] - x[0]).max(0.0), (x[0] - x[1]).max(0.0)));
    let diff = xx.map(|x| (x.0 - x.1));
    let sum = diff.iter().sum::<f64>();
    let diff = diff.map(|x| x - sum / 162.);
    Self::search16(&diff).name("BRUTE61")
}

fn search16(diff: &[f64;162]) -> Decode {
    let mut xxx = [(0f64, 0f64); 81];
    for i in 0..81 {
        let a = diff[i * 2];
        let b = diff[i * 2 + 1];
        xxx[i] = (a, b);
    }

    let mut srs: u32 = 0;
    let mut res: u64 = 0;

    let now = std::time::Instant::now();
    for i in 0..50 {
        let x = &xxx[i..][..24];
        srs = Self::search16a(srs, x);
        res |= (srs as u64 & 1) << i;
    }
    let elapsed = now.elapsed();

    return Self {
        name: "BRUTE16".to_string(),
        snr: SNR { signal: 1.0, noise: 1.0 },
        data: res,
        finished: true,
        iterations: 1,
        duration: elapsed,
    };
}

fn search16a(srs: u32, x: &[(f64,f64)]) -> u32 {
    match x.split_first() {
        None => 0,
        Some(((a,b),y)) => {
            let s0 = srs << 1;
            let m0 = a * par(s0 & POLY1) + b * par(s0 & POLY2) + Self::search16lr(s0, y);
            let s1 = s0 | 1;
            let m1 = a * par(s1 & POLY1) + b * par(s1 & POLY2) + Self::search16lr(s1, y);
            ite(m0 > m1, s0, s1)
        }
    }
}

fn search16lr(srs: u32, x: &[(f64, f64)]) -> f64 {
    match x.split_first() {
        None => 0.0,
        Some(((a,b),y)) => {
            let s0 = srs << 1;
            let m0 = a * par(s0 & POLY1) + b * par(s0 & POLY2) + Self::search16lr(s0, y);
            let s1 = s0 | 1;
            let m1 = a * par(s1 & POLY1) + b * par(s1 & POLY2) + Self::search16lr(s1, y);
            m0.max(m1)
        }
    }
}