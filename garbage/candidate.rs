use std::ops::Range;


use crate::symbols::*;
use crate::ite;
use crate::signal::*;
use num_complex::Complex;
use rustfft::algorithm::Radix4;
use rustfft::*;
use crate::snr::SNR;

#[derive(Debug, Clone, Copy)]
pub struct Candidate {
    pub e: f64,
    pub m: SNR,
    pub f: f64,
    pub d: f64,
    pub t: usize,
}

impl Candidate {
    pub fn zero() -> Self {
        Self {
            e: 0.0,
            m: SNR::zero(),
            f: 0.0,
            d: 0.0,
            t: 0
        }
    }

    pub fn extract(&self, s: &Signal<Complex<f64>>) -> Symbols {
        const F_CENTER: f64 = 1500.0;
        const F_DELTA: f64 = 12000.0 / 8192.0;
        const BIN_COUNT: usize = 256;
        const BLOCK_COUNT: usize = 162;

        let mut results = Symbols::<[f64;4]>::new();
        let mut planner = FftPlanner::<f64>::new();
        let fft = planner.plan_fft_forward(BIN_COUNT);
        let idx = |bin: usize| (bin + BIN_COUNT / 2) % BIN_COUNT;

        let f1 = self.f - F_CENTER + ((BIN_COUNT / 2) as f64) * F_DELTA;
        let f2 = f1 / F_DELTA;
        let bin = f2.ceil();
        let df = bin * F_DELTA - f1;
        let bin = bin as usize;

        let mut s2 = s.clone();
        s2.transpose_and_drift(df, self.d);

        for block in 0..BLOCK_COUNT {
            let bi = self.t + block * BIN_COUNT;
            fft.process(&mut s2[bi..][..BIN_COUNT]);
            // let q0 = s2[bi + idx(bin + 0)].to_polar();
            // let q1 = s2[bi + idx(bin + 1)].to_polar();
            // let q2 = s2[bi + idx(bin + 2)].to_polar();
            // let q3 = s2[bi + idx(bin + 3)].to_polar();
            // println!("{:?} {:?} {:?} {:?}", q0, q1, q2, q3);

            let b0 = s2[bi + idx(bin + 0)].norm();
            let b1 = s2[bi + idx(bin + 1)].norm();
            let b2 = s2[bi + idx(bin + 2)].norm();
            let b3 = s2[bi + idx(bin + 3)].norm();
            results[block] = [b0, b1, b2, b3];
        }
        results
    }

    pub fn refine(&mut self, signal: &Signal<Complex<f64>>) {
        const F_CENTER: f64 = 1500.0;
        const F_DELTA: f64 = 12000.0 / 8192.0;
        const BLOCK_COUNT: usize = 162;
        const BIN_COUNT: usize = 256;
        const ITERATIONS: usize = 8;
    
        let fft = Radix4::<f64>::new(BIN_COUNT, FftDirection::Forward);
        let idx = |bin: usize| (bin + BIN_COUNT / 2) % BIN_COUNT;
    
        let f1 = self.f - F_CENTER + ((BIN_COUNT / 2) as f64) * F_DELTA;
        let f2 = f1 / F_DELTA;
        let bin = f2.ceil();
        let df = bin * F_DELTA - f1;
        let bin = bin as usize;
    
        let mut m_opt: SNR = SNR::zero();
        let mut f_opt: f64 = df;
        let mut d_opt: f64 = self.d;
        let mut t_opt: usize = self.t;
        let mut f_dev: f64 = 0.75;
        let mut d_dev: f64 = 0.5;
        let mut t_dev: usize = 64;

        let mut sym = Symbols::<[f64;4]>::new();
        let mut sig = Signal::<Complex<f64>>::new(375, BIN_COUNT * (BLOCK_COUNT + 3));
        let mut buf = Signal::<Complex<f64>>::new(375, BIN_COUNT * (BLOCK_COUNT + 3));
    
        for _ in 0..ITERATIONS {
            for f in [f_opt - f_dev, f_opt, f_opt + f_dev] {
                for d in [d_opt - d_dev, d_opt, d_opt + d_dev] {
                    sig.copy_from_slice(&signal[..BIN_COUNT * (BLOCK_COUNT + 3)]);
                    sig.transpose_and_drift(f, d);

                    let t1 = ite!(t_dev != 0 && t_opt > t_dev, Some(t_opt - t_dev), None);
                    let t2 = Some(t_opt);
                    let t3 = ite!(t_dev != 0, Some(t_opt + t_dev), None);
                    let ts = [t1, t2, t3];
                    let ts = ts.iter().filter_map(|x|*x);
                    for t in ts {
                        for block in 0..BLOCK_COUNT {
                            let fft_in = &mut sig[BIN_COUNT * block + t..][..BIN_COUNT];
                            let fft_out = &mut buf[BIN_COUNT * block + t..][..BIN_COUNT];
                            fft.process_outofplace_with_scratch(fft_in, fft_out, &mut []);
                        }
                        for block in 0..BLOCK_COUNT {
                            let j = BIN_COUNT * block + t;
    
                            let b0 = buf[j + idx(bin + 0)].norm();
                            let b1 = buf[j + idx(bin + 1)].norm();
                            let b2 = buf[j + idx(bin + 2)].norm();
                            let b3 = buf[j + idx(bin + 3)].norm();
    
                            sym[block] = [b0, b1, b2, b3];
                        }
                        let m = sym.snr_sync2();
                        if m > m_opt {
                            m_opt = m;
                            f_opt = f;
                            d_opt = d;
                            t_opt = t;
                            self.e = sym.energy();
                            self.m = m_opt;
                            self.f = F_CENTER + (bin as f64 - (BIN_COUNT / 2) as f64) * F_DELTA - f;
                            self.d = d;
                            self.t = t;
                        }
                    }
                }
            }
    
            f_dev /= 2.0;
            d_dev /= 2.0;
            t_dev /= 2;
        }
    }
}

pub fn candidates(s: &Signal<Complex<f64>>) -> Vec<Candidate> {
    let candidates = search_candidates(&s);
    let candidates_filtered = filter_candidates(&candidates);

    println!(
        "{} candidates ({} before filtering):",
        candidates_filtered.len(),
        candidates.len()
    );

    candidates_filtered
        .iter()
        .map(|c| {
            // println!(
            //     "M={: >8.1}, t={: >8.1} samples, f={: >8.3} Hz, d={: >8.3}",
            //     c.m.0, c.t, c.f, c.d
            // );
            c.clone()
        })
        .collect()
}

pub fn filter_candidates(cs: &Vec<Candidate>) -> Vec<Candidate> {
    let mut xs: Vec<Option<Candidate>> = cs.iter().cloned().map(Some).collect();
    let mut rs: Vec<Candidate> = Vec::new();
    for i in 0..xs.len() {
        if xs[i].is_some() {
            let mut c = xs[i].take().unwrap();
            for j in i..xs.len() {
                if let Some(o) = &xs[j] {
                    if (o.f - c.f).abs() < 2.0 {
                        let o = xs[j].take().unwrap();
                        if o.m > c.m {
                            c = o;
                        }
                    }
                }
            }
            rs.push(c);
        }
    }
    rs
}

pub fn search_candidates(s: &Signal<Complex<f64>>) -> Vec<Candidate> {
    const F_MIN: f64 = 1390.0;
    const F_MAX: f64 = 1610.0;
    const F_CENTER: f64 = 1500.0;
    const F_DELTA: f64 = 12000.0 / 8192.0;
    const F_STEPS: usize = 10;
    const DRIFT_MAX: f64 = 4.0;
    const DRIFT_STEPS: usize = 20;
    const BLOCK_COUNT: usize = 162;
    const BLOCK_SKEW: usize = 2;
    const BIN_COUNT: usize = 256;
    const BIN_STEPSIZE: usize = 8;
    const BIN_MIN: usize = BIN_COUNT / 2 - ((F_CENTER - F_MIN) / F_DELTA) as usize;
    const BIN_MAX: usize = BIN_COUNT / 2 + ((F_MAX - F_CENTER) / F_DELTA) as usize;

    let mut results = Vec::new();
    let idx = |bin: usize| (bin + BIN_COUNT / 2) % BIN_COUNT;
    let fft = Radix4::<f64>::new(BIN_COUNT, FftDirection::Forward);
    let mut trx = Symbols::<[f64;4]>::new();
    let mut sig = Signal::<Complex<f64>>::new(375, BIN_COUNT * (BLOCK_COUNT + BLOCK_SKEW + 1));
    let mut buf = Signal::<Complex<f64>>::new(375, BIN_COUNT * (BLOCK_COUNT + BLOCK_SKEW + 1));

    for dstep in -(DRIFT_STEPS as isize)..=DRIFT_STEPS as isize {
        let drift = ite(
            dstep == 0,
            0.0,
            dstep as f64 * DRIFT_MAX / DRIFT_STEPS as f64,
        );
        for fstep in 0..F_STEPS {
            let df = fstep as f64 * F_DELTA / F_STEPS as f64;
            sig.copy_from_slice(&s[..BIN_COUNT * (BLOCK_COUNT + BLOCK_SKEW + 1)]);
            sig.transpose_and_drift(df, drift);

            for dt in (0..BIN_COUNT).step_by(BIN_STEPSIZE) {
                for block in 0..BLOCK_COUNT + BLOCK_SKEW {
                    let fft_in = &mut sig[dt..][block * BIN_COUNT..][..BIN_COUNT];
                    let fft_out = &mut buf[block * BIN_COUNT..][..BIN_COUNT];
                    fft.process_outofplace_with_scratch(fft_in, fft_out, &mut []);
                }
                for bin in BIN_MIN..BIN_MAX - 3 {
                    for db in 0..=BLOCK_SKEW {
                        for block in 0..BLOCK_COUNT {
                            let j = db * BIN_COUNT + block * BIN_COUNT;

                            let b0 = buf[j + idx(bin + 0)].norm();
                            let b1 = buf[j + idx(bin + 1)].norm();
                            let b2 = buf[j + idx(bin + 2)].norm();
                            let b3 = buf[j + idx(bin + 3)].norm();

                            trx[block] = [b0, b1, b2, b3];
                        }
                        if !trx.is_plausible() {
                            continue
                        }
                        let candidate = Candidate {
                            e: trx.energy(),
                            m: trx.snr_sync2(),
                            f: F_CENTER + (bin as f64 - (BIN_COUNT / 2) as f64) * F_DELTA
                                - df,
                            d: drift,
                            t: db * BIN_COUNT + dt,
                        };
                        results.push(candidate);
                    }
                }
            }
        }
    }

    results.sort_by(|a, b| b.m.ratio().total_cmp(&a.m.ratio()));
    results
}

pub fn best_candidate(s: &Signal<Complex<f64>>) -> Candidate {
    const F_MIN: f64 = 1390.0;
    const F_MAX: f64 = 1610.0;
    const F_CENTER: f64 = 1500.0;
    const F_DELTA: f64 = 12000.0 / 8192.0;
    const F_STEPS: usize = 5;
    const DRIFT_MAX: f64 = 4.0;
    const DRIFT_STEPS: usize = 3;
    const BLOCK_COUNT: usize = 162;
    const BLOCK_SKEW: usize = 2;
    const BIN_COUNT: usize = 256;
    const BIN_STEPSIZE: usize = 64;
    const BIN_MIN: usize = BIN_COUNT / 2 - ((F_CENTER - F_MIN) / F_DELTA) as usize;
    const BIN_MAX: usize = BIN_COUNT / 2 + ((F_MAX - F_CENTER) / F_DELTA) as usize;

    let mut best = Candidate::zero();
    let idx = |bin: usize| (bin + BIN_COUNT / 2) % BIN_COUNT;
    let fft = Radix4::<f64>::new(BIN_COUNT, FftDirection::Forward);
    let mut trx = Symbols::<[f64;4]>::new();
    let mut sig = Signal::<Complex<f64>>::new(375, BIN_COUNT * (BLOCK_COUNT + BLOCK_SKEW + 1));
    let mut buf = Signal::<Complex<f64>>::new(375, BIN_COUNT * (BLOCK_COUNT + BLOCK_SKEW + 1));
    let mut iterations = 0;

    for dstep in -(DRIFT_STEPS as isize)..=DRIFT_STEPS as isize {
        let drift = ite(
            dstep == 0,
            0.0,
            dstep as f64 * DRIFT_MAX / DRIFT_STEPS as f64,
        );
        for fstep in 0..F_STEPS {
            let df = fstep as f64 * F_DELTA / F_STEPS as f64;
            sig.copy_from_slice(&s[..BIN_COUNT * (BLOCK_COUNT + BLOCK_SKEW + 1)]);
            sig.transpose_and_drift(df, drift);

            for dt in (0..BIN_COUNT).step_by(BIN_STEPSIZE) {
                //println!("dstep={}, fstep={}, dt={}, block={}", dstep, fstep, dt, 0);
                for block in 0..BLOCK_COUNT + BLOCK_SKEW {
                    let fft_in = &mut sig[dt..][block * BIN_COUNT..][..BIN_COUNT];
                    let fft_out = &mut buf[block * BIN_COUNT..][..BIN_COUNT];
                    fft.process_outofplace_with_scratch(fft_in, fft_out, &mut []);
                }
                for bin in BIN_MIN..BIN_MAX - 3 {
                    for db in 0..=BLOCK_SKEW {
                        iterations += 1;
                        for block in 0..BLOCK_COUNT {
                            let j = db * BIN_COUNT + block * BIN_COUNT;

                            let b0 = buf[j + idx(bin + 0)].norm();
                            let b1 = buf[j + idx(bin + 1)].norm();
                            let b2 = buf[j + idx(bin + 2)].norm();
                            let b3 = buf[j + idx(bin + 3)].norm();

                            trx[block] = [b0, b1, b2, b3];
                        }
                        let snr = trx.snr_sync2();
                        if snr > best.m {
                            best.e = trx.energy();
                            best.m = snr;
                            best.f = F_CENTER + (bin as f64 - (BIN_COUNT / 2) as f64) * F_DELTA
                                - df;
                            best.d = drift;
                            best.t = db * BIN_COUNT + dt;
                        }
                    }
                }
            }
        }
    }

    //println!("ITER: {}", iterations);
    println!("BEST {:?}", best);

    best
}

