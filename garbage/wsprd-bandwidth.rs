use clap::Parser;
use num_complex::Complex;
use phenol::message;
use phenol::signal::*;
use phenol::candidate::*;
use phenol::plot::*;
use phenol::phanol;
use phenol::symbols::Symbols;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(value_name = "FILE.wav")]
    file: String,
    // #[arg(short, long, default_value_t = 1)]
    // count: u8,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const RATE: usize = 12_000;
    const SAMPLES: usize = 8_192 * 162;

    let mut plan = rustfft::FftPlanner::<f64>::new();
    let fft_8192 = plan.plan_fft_inverse(8192);

    let n: u64 = 0b11101010001101111111100100010011011101101001101111;
    let fs: f64 = 0.5;
    let sym = Symbols::<u8>::encode(n);
    let tone = Signal::new_const(RATE, 8192, Complex::new(0.0, 0.0));
    let mut tones = [tone.clone(), tone.clone(), tone.clone(), tone.clone()];
    for i in 0..4 {
        tones[i][1024 + i] = Complex::new(1.0, 0.0);
        fft_8192.process(&mut tones[i]);
    }

    let mut s = Signal::new_const(RATE, SAMPLES, Complex::new(0.0,0.0));
    for i in 0..162 {
        let r = rand::random::<f64>();
       // println!("{:?}", r);
        for j in 0..8192 {
            s[i*8192 + j] = tones[sym[i] as usize][j];
        }
    }

    let mut plan = rustfft::FftPlanner::<f64>::new();
    let fft_all = plan.plan_fft_forward(SAMPLES);
    fft_all.process(&mut s);
    for x in s.iter_mut() {
        x.re = x.norm();
        x.re = x.re*x.re;
        x.im = 0.0;
    }

    let mut s = s.realize();
    s.normalize(1.0);



    let q = 1000;
    for i in (1024*162 - q) .. (1024*162 + q) {
        println!("{} {}", i, s[i])
    }

    let w = 4_000;
    let v = 600;
    let mut all = 0.0;
    for i in (1024*162 - w) .. (1024*162 - v) {
        all += s[i]*s[i];
    }
    for i in (1024*162 + v) .. (1024*162 + w) {
        all += s[i]*s[i];
    }
    let mut sub = 0.0;
    for i in ((1024*162) - v) .. ((1024*162) + v) {
        sub += s[i]*s[i];
    }
    
    println!("ALL {}, SUB {}", all, sub);
    
    s.save("bandwidth.wav")?;
    Ok(())
}
