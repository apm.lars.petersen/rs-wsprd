use crate::symbols::Symbols;
use std::collections::BTreeMap;
use std::collections::VecDeque;

const POLY1: u32 = 0xF2D05351;
const POLY2: u32 = 0xE4613C47;

pub fn decode(os: &Symbols<[f64; 2]>) -> Option<(f64, u64)> {
    const QUEUE_SIZE_MAX: usize = 65_536;

    let f = |x: f64| x.min(0.0);

    let mut h = VecDeque::with_capacity(QUEUE_SIZE_MAX);
    h.push_back((Sync::default(), Node2::default()));
    for _ in 0..QUEUE_SIZE_MAX {
        let (_, mut n) = h.pop_front().unwrap();
        let i = n.i;
        let m = n.m;
        let a = os[i * 2][1] - os[i * 2][0];
        let b = os[i * 2 + 1][1] - os[i * 2 + 1][0];

        n.i += 1;
        n.s <<= 1;
        n.m = m + f(par(n.s & POLY1) * a) + f(par(n.s & POLY2) * b);
        h.push_back((Sync(n.m / n.i as f64), n));

        n.s |= 1;
        n.r |= 1 << i;
        n.m = m + f(par(n.s & POLY1) * a) + f(par(n.s & POLY2) * b);
        h.push_back((Sync(n.m / n.i as f64), n));
    }

    let mut q: BTreeMap<Sync, Node2> = h.into_iter().collect();
    while let Some((_, mut n)) = q.pop_first() {
        if n.i == 80 {
            //n.m /= os.iter().map(|x| x.abs()).sum::<f64>();
            return Some(n).filter(|n| n.r != 0).map(|n| (n.m, n.r));
        }

        let i = n.i;
        let m = n.m;
        // let a = os[i * 2];
        // let b = os[i * 2 + 1];
        let a = os[i * 2][1] - os[i * 2][0];
        let b = os[i * 2 + 1][1] - os[i * 2 + 1][0];

        n.i += 1;
        n.s <<= 1;
        n.m = m + f(par(n.s & POLY1) * a) + f(par(n.s & POLY2) * b);
        q.insert(Sync(n.m / n.i as f64), n);

        if i < 50 {
            n.s |= 1;
            n.r |= 1 << i;
            n.m = m + f(par(n.s & POLY1) * a) + f(par(n.s & POLY2) * b);
            q.insert(Sync(n.m / n.i as f64), n);
            if q.len() > QUEUE_SIZE_MAX {
                q.pop_last();
            }
        }
    }

    None
}

#[derive(Debug, Default, Clone, Copy)]
struct Node2 {
    i: usize,   // Index [0..80]
    s: u32,     // Shift register state
    r: u64,     // Result bits whereas bit 0 = LSB, bit 49 at << 49
    m: f64,     // Path metric up to this node
}

#[derive(Debug, Default, Clone, Copy, PartialEq, PartialOrd)]
struct Sync(f64);

impl Eq for Sync {}
impl Ord for Sync {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.0.total_cmp(&self.0)
    }
}

#[inline(always)]
fn par(x: u32) -> f64 {
    if x.count_ones() % 2 != 0 {
        1.0
    } else {
        -1.0
    }
}
