use num_complex::Complex64;
use crate::constants::SYNC;
use crate::{signal::*, snr::SNR};
use rustfft::{algorithm::Radix4, num_traits::Zero, Fft, FftDirection};

pub const SYMBOL_COUNT: usize = 162;
pub const SYMBOL_EXTRA: usize = 3;
pub const SYMBOL_TOTAL: usize = SYMBOL_COUNT + SYMBOL_EXTRA;

pub const SAMPLE_RATE: usize = 12_000;
pub const SAMPLE_SYMBOL: usize = 8_192;

pub const F_CENTER: f64 = 1_500.0;
pub const F_TONE: f64 = SAMPLE_RATE as f64 / SAMPLE_SYMBOL as f64;
pub const F_BAND: f64 = 200.0;

pub const BIN_COUNT: usize = (F_BAND / F_TONE) as usize;
pub const BIN_CENTER: usize = (F_CENTER / F_TONE) as usize;
pub const BIN_LO: usize = BIN_CENTER - BIN_COUNT / 2;
pub const BIN_HI: usize = BIN_CENTER + BIN_COUNT / 2;

fn check(condition: bool) -> Option<()> {
    if condition {
        Some(())
    } else {
        None
    }
}

pub struct Spectrum2 {
    nf: usize,
    nd: usize,
    data: Vec<Candidate>
}

impl Spectrum2 {
    pub fn new(nf: usize, nd: usize) -> Self {
        Self {
            nf,
            nd,
            data: vec![Candidate::default(); nf * nd]
        }
    }

    pub fn get(&self, f: usize, d: usize) -> Option<&Candidate> {
        check(f < self.nf && d < self.nd)?;
        let i = self.nf * f + d;
        Some(&self.data[i])
    }

    pub fn get_mut(&mut self, f: usize, d: usize) -> Option<&mut Candidate> {
        check(f < self.nf && d < self.nd)?;
        let i = self.nf * f + d;
        Some(&mut self.data[i])
    }
}

pub struct Matrix {
    pub data: Box<[[Complex64; BIN_COUNT]; SYMBOL_TOTAL]>,
}

impl Matrix {
    pub fn from_signal(signal: &[Complex64]) -> Self {
        let fft = Radix4::<f64>::new(8192, FftDirection::Forward);
        let mut fft_buf: [Complex64; 8192] = [Complex64::new(0.0, 0.0); 8192];
        let mut fft_scratch: [Complex64; 8192] = [Complex64::new(0.0, 0.0); 8192];

        let mut self_ = Self::default();

        for i in 0..SYMBOL_TOTAL {
            fft_buf.copy_from_slice(&signal[i * SAMPLE_SYMBOL..][..SAMPLE_SYMBOL]);
            fft.process_with_scratch(&mut fft_buf, &mut fft_scratch);
            self_.data[i].copy_from_slice(&fft_buf[BIN_LO..][..BIN_COUNT]);
        }

        self_
    }

    pub fn plot(&self, filename: &str) {
        self.save("gnuplot/matrix.csv");

        let mut cmd = String::new();
        cmd.push_str(&format!("set terminal svg size 1200,1200 dynamic enhanced font 'arial,10' mousing name \"foobar\" butt dashlength 1.0\n"));
        cmd.push_str(&format!("set output '{}'\n", filename));
        cmd.push_str(&format!("set xr [-0.5:136.5]\n"));
        cmd.push_str(&format!("set yr [-0.5:162.5]\n"));
        cmd.push_str(&format!("set size ratio 1.0\n"));
        cmd.push_str(&format!(
            "plot 'gnuplot/matrix.csv' matrix with image pixels\n"
        ));

        std::fs::write("gnuplot/cmd.gnuplot", cmd).unwrap();
        std::process::Command::new("gnuplot")
            .arg("-c")
            .arg("gnuplot/cmd.gnuplot")
            .output()
            .expect("failed to execute process");
    }

    fn save(&self, filename: &str) {
        let mut dat = String::new();
        for s in self.data.iter() {
            for i in s.iter() {
                dat.push_str(&format!("{:8.5} ", i.norm()));
            }
            dat.push_str("\n");
        }
        std::fs::write(filename, dat).unwrap();
    }
}

impl Default for Matrix {
    fn default() -> Self {
        Self {
            data: Box::new([[Complex64::zero(); BIN_COUNT]; SYMBOL_TOTAL]),
        }
    }
}

pub struct Spectrum<const BINS: usize> {
    data: Box<[[Candidate; Section::RANGE]; BINS]>
}

impl <const BINS: usize> Spectrum<BINS> {
    pub fn new() -> Self {
        Self {
            data: Box::new([[Candidate::default(); Section::RANGE]; BINS])
        }
    }

    pub fn insert(&mut self, s: &[Complex64], sect_freq: Section, sect_time: Section) {
        let matrix = Matrix::from_signal(s);

        for off_bins in 0..=BIN_COUNT - 4 {
            for off_symbols in 0..=SYMBOL_EXTRA {
                let symbols = Symbols::new(&matrix, off_symbols, off_bins);
                let snr = symbols.snr();

                let c = &mut self.data[off_bins][sect_freq.0];

                if snr > c.snr {
                    c.snr = snr;
                    c.off_symbols = off_symbols;
                    c.off_samples = sect_time.as_samples();
                }
            }
        }
    }

    pub fn print(&self) {
        for (i,bin) in self.data.iter().enumerate().skip(19).take(4) {
            println!("BIN {}", i);
            for (j,cand) in bin.iter().enumerate() {
                if cand.snr != SNR(0.0) {
                    println!("CAND {:4.0}: {:3.8} ({}, {})", j, cand.snr.0, cand.off_symbols, cand.off_samples)
                }
            }
        }
    }

    pub fn print_maxima(&self) {
        let mut left;
        let mut center = Candidate::default();
        let mut right = Candidate::default();

        for (i,bin) in self.data.iter().enumerate() {
            for (j,cand) in bin.iter().enumerate() {
                if cand.snr != SNR(0.0) {
                    left = center;
                    center = right;
                    right = *cand;
                    if left.snr < center.snr && center.snr < right.snr {
                        println!("MAX {:4.0}.{:4.0}: {:3.8} ({}, {})", i, j, center.snr.0, center.off_symbols, center.off_samples)
                    }
                }
            }
        }
    }
}

#[derive(Debug, Clone, Copy, Default)]
pub struct Candidate {
    pub snr: SNR,
    pub off_symbols: usize,
    pub off_samples: usize,
}

impl PartialEq for Candidate {
    fn eq(&self, other: &Self) -> bool {
        self.snr == other.snr
    }
}

impl PartialOrd for Candidate {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for Candidate {}

impl Ord for Candidate {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.snr.0.total_cmp(&other.snr.0)
    }
}

pub struct Symbols<'a> {
    matrix: &'a Matrix,
    off_symbols: usize,
    off_bins: usize,
}

impl<'a> Symbols<'a> {
    pub fn new(matrix: &'a Matrix, off_symbols: usize, off_bins: usize) -> Self {
        Self {
            matrix,
            off_bins,
            off_symbols,
        }
    }

    pub fn snr(&self) -> SNR {
        let mut signal = 0f64;
        let mut noise = 0f64;

        for i in 0..SYMBOL_COUNT {
            let e = self.matrix.data[i + self.off_symbols];
            let o0 = e[self.off_bins + 0];
            let o1 = e[self.off_bins + 1];
            let o2 = e[self.off_bins + 2];
            let o3 = e[self.off_bins + 3];

            let x0 = o0 + o2;
            let x1 = o1 + o3;

            let (s, n) = match SYNC[i] {
                0 => (x0 - x1, x1 + x1),
                _ => (x1 - x0, x0 + x0),
            };

            signal += s.norm();
            noise += n.norm();
        }

        SNR(signal / noise)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Section(pub usize);

impl Section {
    const RANGE: usize = 256;
    const SAMPLE_RATE: usize = 12_000;
    const SYMBOL_RATE: usize = 8_192;

    pub fn as_samples(&self) -> usize {
        const FACTOR: usize = Section::SYMBOL_RATE / Section::RANGE;
        self.0 * FACTOR
    }

    pub fn as_hertz(&self) -> f64 {
        const HZ_MAX: f64 = Section::SAMPLE_RATE as f64 / Section::SYMBOL_RATE as f64;
        (self.0 as f64 / Section::RANGE as f64) * HZ_MAX
    }
}

#[derive(Debug)]
pub struct BisectIterator {
    stepsize: usize,
    position: usize,
}

impl BisectIterator {
    pub fn new(iteration: usize) -> Self {
        let x = Section::RANGE >> iteration;
        Self {
            stepsize: x * 2,
            position: x % Section::RANGE,
        }
    }
}

impl Iterator for BisectIterator {
    type Item = Section;

    fn next(&mut self) -> Option<Self::Item> {
        let p = self.position;
        if p >= Section::RANGE {
            None
        } else {
            self.position += self.stepsize;
            Some(Section(p))
        }
    }
}
