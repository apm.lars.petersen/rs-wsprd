#![feature(portable_simd)]
use clap::Parser;
use num_complex::Complex;
use num_complex::Complex32;
use num_complex::Complex64;
use phenol::signal::*;
use phenol::symbols::Symbols;
use rustfft::algorithm::Radix4;
use rustfft::Fft;
use rustfft::FftDirection;
use std::simd::num::SimdFloat;
use std::simd::StdFloat;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(value_name = "FILE.wav")]
    file: String,
    // #[arg(short, long, default_value_t = 1)]
    // count: u8,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    let n: u64 = 0b11101010001101111111100100010011011101101001101111;

    let raw = Signal::<f32>::load(&args.file)?;

    scan(&raw);

    let now = std::time::Instant::now();
    let r = refine_simd_f32(&raw);
    let elapsed = now.elapsed();
    println!("ELAPSED: {:?}", elapsed);
    // let x = Symbols::<u8>::encode(n);

    // for i in 0..162 {
    //     let p0 = r[i][0].to_polar();
    //     let p1 = r[i][1].to_polar();
    //     let p2 = r[i][2].to_polar();
    //     let p3 = r[i][3].to_polar();
    //     print!("{:8.2} {:8.2}", p0.0, p0.1);
    //     print!(" | {:8.2} {:8.2}", p1.0, p1.1);
    //     print!(" | {:8.2} {:8.2}", p2.0, p2.1);
    //     print!(" | {:8.2} {:8.2}", p3.0, p3.1);
    //     println!(" | {:4.0}", x[i]);
    // }

    // let mut m = 0f32;
    // let mut a = Complex64::new(0.0,0.0);
    // for j in 0..4 {
    //     for i in 0..162 {
    //         m += r[i][j].norm();
    //         a += Complex64::new(r[i][j].re as f64, r[i][j].im as f64);
    //     }
    //     println!("{} {:8.2} {:8.2}", j, a.norm(), m);
    // }

    // let avg = r.iter().map(|x|x.iter().max_by(|a,b|a.norm().total_cmp(&b.norm())).copied().map(|x|x.norm()).unwrap_or_default()).sum::<f32>() / 162.0;

    // println!("SNR SYNC:       {:8.4}", r.snr_sync());
    // println!("SNR DATA:       {:8.4}", r.snr_data(n));
    // println!("SNR SYNC SUPER: {:8.4}", r.snr_sync_super());
    // println!("SNR DATA SUPER: {:8.4}", r.snr_data_super(n));
    // println!("AVG:            {:8.4}", avg);

    // let s = raw.extract_wspr();

    // let now = std::time::Instant::now();
    // let mut best = best_candidate(&s);
    // println!("BEFORE {:?}", best);
    // //best.refine(&s);
    // println!("AFTER {:?}", best);
    // let sym = best.extract(&s);
    // println!("SYMBOLS:");
    // for i in 0..162 {
    //     println!("{:10.2} {:10.2} {:10.2} {:10.2}", sym[i][0], sym[i][1], sym[i][2], sym[i][3]);
    // }
    // plot("plots/best.svg", &sym);

    Ok(())
}

fn scan(s: &Signal<f32>) {
    const N: usize = 8192;

    let fftf = Radix4::<f32>::new(N, FftDirection::Forward);
    let mut xs = vec![Complex32::default(); 162 * N];

    for i in 0..162*N {
        xs[i].re = s[i];
    }

    let now = std::time::Instant::now();
    for i in 0..162 {
        fftf.process(&mut xs[N*i..N*(i+1)]);
    }
    let elapsed = now.elapsed();

    for i in 1000..1050 {
        println!("{:?}", xs[i]);
    }

    println!("ELAPSED: {:?}", elapsed);
    
    let mut m = 0f32;
    let mut a = Complex64::new(0.0,0.0);
    for j in 10..14 {
        for i in 0..162 {
            let z = (i * N) + 480 + j;
            m += xs[z].norm();
            a += Complex64::new(xs[z].re as f64, xs[z].im as f64);
        }
        println!("{} {:8.2} {:8.2}", j, a.norm(), m);
    }
    println!("{} {:8.2} {:8.2}", 0, a.norm(), m);
}

fn refine_simd_f32(s: &Signal<f32>) -> Symbols<[Complex<f32>;4]> {
    type F32xn = std::simd::f32x32;
    const N: usize = 8192;
    const XN: usize = std::mem::size_of::<F32xn>() / std::mem::size_of::<f32>();
    const IT: usize = 8192 / XN;
    const RATE: usize = 12_000;

    let fftf = Radix4::<f32>::new(N, FftDirection::Forward);
    let ffti = Radix4::<f32>::new(N, FftDirection::Inverse);
    let mut tones = [
        [Complex::new(0.0, 0.0); N],
        [Complex::new(0.0, 0.0); N],
        [Complex::new(0.0, 0.0); N],
        [Complex::new(0.0, 0.0); N],
    ];
    for i in 0..4 {
        tones[i][500 + i] = Complex::new(1.0, 0.0);
        ffti.process(&mut tones[i]);
    }

    let t_off = 0;

    let mut sym = Symbols([[Complex::<f32>::new(0.0, 0.0);4]; 162]);

    let mut tre = [[[0f32; XN]; IT];4];
    let mut tim = [[[0f32; XN]; IT];4];

    for t in 0..4 {
        for i in 0..IT {
            for j in 0..XN {
                tre[t][i][j] = tones[t][XN * i + j].re;
                tim[t][i][j] = tones[t][XN * i + j].im;
            }
        }
    }

    let now = std::time::Instant::now();

    for t in 0..4 {
        for i in 0..162 {
            let o = N * i + t_off;
            let mut re = F32xn::default();
            let mut im = F32xn::default();
            let si: &[f32; N] = s[o..][..N].try_into().unwrap();
            for j in 0..IT {
                let sj = F32xn::from_slice(&si[XN * j..]);
                re = F32xn::from_array(tre[t][j]).mul_add(sj, re);
                im = F32xn::from_array(tim[t][j]).mul_add(sj, im);
            }
            sym[i][t].re = re.reduce_sum();
            sym[i][t].im = im.reduce_sum();
        }
    }

    println!("ELLA {:?}", now.elapsed());

    sym
}
