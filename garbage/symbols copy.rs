use num_complex::Complex32;
use num_complex::Complex64;

use crate::constants::{POLY1, POLY2, SYNC};
use crate::snr::SNR;
use std::ops::Deref;
use std::ops::DerefMut;

#[derive(Debug, Copy, Clone)]
pub struct Symbols<T = [f64; 4]>(pub [T; 162]);

impl<T: Default + Clone + Copy> Symbols<T> {
    pub fn new() -> Self {
        Self([Default::default(); 162])
    }

    pub fn shuffle(&mut self) {
        let mut tmp = [T::default(); 162];
        let mut p: usize = 0;
        let mut i: u8 = 0;
        let mut j: u8 = 0;

        while p < 162 {
            j = i.reverse_bits();
            if j < 162 {
                tmp[j as usize] = self[p];
                p = p + 1;
            }
            i = i + 1;
        }
        for i in 0..162 {
            self[i] = tmp[i];
        }
    }

    pub fn unshuffle(&mut self) {
        let mut tmp = [T::default(); 162];
        let mut p: usize = 0;
        let mut i: u8 = 0;
        let mut j: u8 = 0;

        while p < 162 {
            j = i.reverse_bits();
            if j < 162 {
                tmp[p] = self[j as usize];
                p = p + 1;
            }
            i = i + 1;
        }
        for i in 0..162 {
            self[i] = tmp[i];
        }
    }
}

impl Symbols<u8> {
    pub fn encode(n: u64) -> Self {
        let mut x = Symbols([0u8; 162]);
        let mut n = n;
        let mut s = 0u32;

        for i in 0..81 {
            s <<= 1;
            s |= n as u32 & 1;
            n >>= 1;
            x[i * 2 + 0] = (s & POLY1).count_ones() as u8 % 2;
            x[i * 2 + 1] = (s & POLY2).count_ones() as u8 % 2;
        }

        x.shuffle();

        for i in 0..162 {
            x[i] = 2 * x[i] + SYNC[i] as u8;
        }

        x
    }
}

impl Symbols<f64> {
    pub fn bits(&self) -> [u8; 162] {
        let mut x = [0; 162];
        for i in 0..162 {
            x[i] = if self[i] > 0.0 { 1 } else { 0 }
        }
        x
    }

    pub fn nodc(&mut self) {
        let sum = self.iter().sum::<f64>() / 162.0;
        self.iter_mut().for_each(|x| *x -= sum);
    }

    pub fn normalize(&mut self) {
        let dc = self.iter().sum::<f64>() / 162.0;
        self.0.iter_mut().for_each(|x| *x -= dc);
    }
}

impl Symbols<[Complex64;4]> {
    pub fn snr(&self) -> f64 {
        let mut signal = 0f64;
        let mut noise = 0f64;
        for (i, x) in self.0.iter().enumerate() {
            let n0 = (x[0] + x[2]).norm_sqr();
            let n1 = (x[1] + x[3]).norm_sqr();

            let (s, n) = match SYNC[i] {
                0 => (n0 - n1, n1 + n1),
                _ => (n1 - n0, n0 + n0),
            };

            signal += s;
            noise += n;
        }
        signal / noise
    }

    /// Compute the left-right-ratio
    ///
    /// Rule of thumb: The higher the LRR the worse the signal.
    /// 
    /// The LRR is an indicator for being 2 bins off the actual signal.
    /// In this case the correlation with the sync vector is very high although
    /// it is not a desired match as half the information is missing.
    /// 
    /// The LRR is computed as `max(e01/e23, e23/e01)` with
    /// `eXY` as the enery sum of both the left resp. right tone. 
    pub fn lrr(&self) -> f64 {
        let mut l = 0.0;
        let mut r = 0.0;
        for x in self.0 {
            l += x[0].norm_sqr() + x[1].norm_sqr();
            r += x[2].norm_sqr() + x[3].norm_sqr();
        }
        (l / r).max(r / l)
    }
}

impl Symbols<[Complex32;4]> {
    pub fn snr_sync(&self) -> f32 {
        let mut signal = 0f32; //Complex32::default();
        let mut noise = 0f32; //Complex32::default();
        for (i, e) in self.0.iter().enumerate() {
            let o0 = e[0];
            let o1 = e[1];
            let o2 = e[2];
            let o3 = e[3];

            let x0 = o0 + o2;
            let x1 = o1 + o3;

            let (s,n) = match SYNC[i] {
                0 => (x0 - x1 / 2.0, x1),
                _ => (x1 - x0 / 2.0, x0)
            };

            signal += s.norm();
            noise += n.norm();
        }
        signal / noise
    }

    pub fn snr_sync_super(&self) -> f32 {
        let mut signal = 0f32;
        let mut noise = 0f32;
        for (i, e) in self.0.iter().enumerate() {
            let o0 = e[0];
            let o1 = e[1];
            let o2 = e[2];
            let o3 = e[3];

            let x0 = o0 + o2;
            let x1 = o1 + o3;

            let (s,n) = match SYNC[i] {
                0 => (x0, x1),
                _ => (x1, x0)
            };

            signal += s.norm();
            noise += n.norm();
        }
        signal / noise
    }

    pub fn snr_data(&self, n: u64) -> f32 {
        let x = Symbols::<u8>::encode(n);
        let mut noise = 0.0;
        let mut signal = 0.0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let n = (os[b] + os[c] + os[d]) / 3.0;
            let s = os[a] - n;
            signal += s.norm();
            noise += n.norm();
        }
        signal / noise
    }

    pub fn snr_data_super(&self, n: u64) -> f32 {
        let x = Symbols::<u8>::encode(n);
        let mut noise = 0.0;
        let mut signal = 0.0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let s = os[a];
            let n = (os[b] + os[c] + os[d]) / 3.0;
            signal += s.norm();
            noise += n.norm();
        }
        signal / noise
    }
}

impl Symbols<[f64; 4]> {
    // pub fn new() -> Self {
    //     Self([[0f64; 4]; 162])
    // }

    pub fn sum(&self) -> f64 {
        self.iter().map(|x| x.iter().sum::<f64>()).sum::<f64>()
    }

    pub fn sums(&self) -> [f64;4] {
        let mut sums = [0f64;4];
        for e in self.iter() {
            for i in 0..4 {
                sums[i] += e[i];
            }
        }
        sums
    }

    pub fn means(&self) -> [f64;4] {
        let mut sums = self.sums();
        for i in 0..4 {
            sums[i] /= 162.0;
        }
        sums
    }

    pub fn rmss(&self) -> [f64;4] {
        let mut sums = [0f64;4];
        for e in self.iter() {
            for i in 0..4 {
                sums[i] += e[i] * e[i];
            }
        }
        for i in 0..4 {
            sums[i] = (sums[i] / 162.0).sqrt();
        }
        sums
    }

    pub fn variances(&self) -> [f64;4] {
        let means = self.means();
        let mut variances = [0f64;4];
        for e in self.iter() {
            for i in 0..4 {
                let a = means[i] - e[i];
                variances[i] += a * a;
            }
        }
        for i in 0..4 {
            variances[i] = (variances[i] / 162.0).sqrt();
        }
        variances
    }

    pub fn min(&self) -> f64 {
        self.iter().map(|x| x.iter().copied().min_by(f64::total_cmp).unwrap_or_default()).min_by(f64::total_cmp).unwrap_or_default()
    }

    pub fn squared(&mut self) {
        for i in 0..162 {
            for j in 0..4 {
                self[i][j] *= self[i][j];
            }
        }
    }

    pub fn data(&self) -> Symbols<[f64; 2]> {
        let mut d = Symbols::<[f64; 2]>::new();
        for j in 0..162 {
            if SYNC[j] == 0 {
                d[j] = [self[j][0], self[j][2]];
            } else {
                d[j] = [self[j][1], self[j][3]];
            }
        }
        d
    }

    pub fn data_diff(&self) -> Symbols<f64> {
        let mut d = Symbols([0f64; 162]);
        for j in 0..162 {
            if SYNC[j] == 0 {
                d[j] = self[j][2] - self[j][0];
            } else {
                d[j] = self[j][3] - self[j][1];
            }
        }
        d
    }

    pub fn energy(&self) -> f64 {
        self.0.iter().map(|x| x.iter().sum::<f64>()).sum()
    }

    pub fn encode(n: u64) -> Self {
        let x = Symbols::<u8>::encode(n);
        let mut y = Self::new();

        for i in 0..162 {
            y[i][x[i] as usize] = 1.0;
        }

        y
    }

    pub fn snr_sync(&self) -> SNR {
        let mut signal = 0f64;
        let mut noise = 0f64;
        for (i, e) in self.0.iter().enumerate() {
            let o0 = e[0];
            let o1 = e[1];
            let o2 = e[2];
            let o3 = e[3];

            let x0 = o0 + o2;
            let x1 = o1 + o3;

            let (s,n) = match SYNC[i] {
                0 => (x0 - x1, x1 + x1),
                _ => (x1 - x0, x0 + x0)
            };

            signal += s;
            noise += n;
        }
        SNR { signal, noise }
    }

    pub fn snr_sync2(&self) -> SNR {
        let mut signal = 0f64;
        let mut noise = 0f64;
        for (i, e) in self.0.iter().enumerate() {
            let o0 = e[0] * e[0];
            let o1 = e[1] * e[1];
            let o2 = e[2] * e[2];
            let o3 = e[3] * e[3];

            let x0 = o0 + o2;
            let x1 = o1 + o3;

            let (s,n) = match SYNC[i] {
                0 => (x0 - x1, x1 + x1),
                _ => (x1 - x0, x0 + x0)
            };

            signal += s;
            noise += n;
        }
        signal = signal.max(0.0);
        SNR { signal: signal.sqrt(), noise: noise.sqrt() }
    }

    pub fn snr_data(&self, n: u64) -> SNR {
        let x = Symbols::<u8>::encode(n);
        let mut noise = 0.0;
        let mut signal = 0.0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let n = (os[b] + os[c] + os[d]) / 3.0;
            let s = os[a] - n;
            noise += n;
            signal += s;
        }
        SNR { signal, noise }
    }

    pub fn snr_data2(&self, n: u64) -> SNR {
        let x = Symbols::<u8>::encode(n);
        let mut noise = 0.0;
        let mut signal = 0.0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let n = (os[b] * os[b] + os[c] * os[c] + os[d] * os[d]) / 3.0;
            let s = os[a] * os[a] - n;
            noise += n;
            signal += s;
        }
        SNR { signal: signal.sqrt(), noise: noise.sqrt() }
    }

    pub fn snr_data_corr(&self, n: u64) -> SNR {
        let x = Symbols::<u8>::encode(n);
        let mut noise = 0.0;
        let mut signal = 0.0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let n = (os[b] + os[c] + os[d]) / 3.0;
            let s = os[a] - n;
            signal += s;
            noise += s.abs();
        }
        SNR { signal, noise }
    }

    pub fn snr_data_corr2(&self, n: u64) -> SNR {
        let x = Symbols::<u8>::encode(n);
        let mut noise = 0.0;
        let mut signal = 0.0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let n = (os[b] + os[c] + os[d]) / 3.0;
            let s = os[a] - n;
            signal += (s * s) * s.signum();
            noise += s * s ;
        }
        SNR { signal, noise }
    }

    pub fn errors(&self, n: u64) -> usize {
        let x = Symbols::<u8>::encode(n);
        let mut errors = 0;
        for (os, es) in self.iter().zip(x.iter()) {
            let mut x: [(usize, f64);4] = [(0,os[0]), (1,os[1]), (2,os[2]), (3,os[3])];
            x.sort_by(|a,b|b.1.total_cmp(&a.1));
            if x[0].0 != *es as usize {
                errors += 1;
            }
        }
        errors
    }

    pub fn errors2(&self, n: u64) -> usize {
        let x = Symbols::<u8>::encode(n);
        let mut errors = 0;
        for (os, expected) in self.iter().zip(x.iter()) {
            let (a, b, c, d) = match expected {
                0 => (0, 1, 2, 3),
                1 => (1, 0, 2, 3),
                2 => (2, 0, 1, 3),
                _ => (3, 0, 1, 2)
            };
            let n = (os[b] + os[c] + os[d]) / 3.0;
            let s = os[a] - n;
            if s < 0.0 {
                errors += 1;
            }
        }
        errors
    }

    pub fn normalize(&mut self) {
        for e in self.iter_mut() {
            for i in 0..4 {
                e[i] = e[i] * e[i];
            }
            let mean = e.iter().sum::<f64>() / 4.0;
            e.iter_mut().for_each(|x| { *x -= mean; let y = x.signum(); *x = x.abs().sqrt() * y; });
        }
        // self.squared();
        // let mean = self.sum() / 162.0;
        // self.iter_mut().for_each(|x| x.iter_mut().for_each(|y| *y -= mean));
        // let min = self.min();
        // self.iter_mut().for_each(|x| x.iter_mut().for_each(|y| *y += min));
    }

    pub fn stats(&self, data: u64) -> Stats {
        Stats {
            energy: self.energy(),
            sums: self.sums(),
            means: self.means(),
            rmss: self.rmss(),
            variances: self.variances(),
            snr_sync: self.snr_sync(),
            snr_sync2: self.snr_sync2(),
            snr_data: self.snr_data(data),
            snr_data2: self.snr_data2(data),
            snr_data_corr: self.snr_data_corr(data),
            snr_data_corr2: self.snr_data_corr2(data),
            errors: self.errors(data),
            errors2: self.errors2(data)
        }
    }

    pub fn is_plausible(&self) -> bool {
        let mut sums = [0f64; 4];
        for e in self.0 {
            for i in 0..4 {
                sums[i] += e[i];
            }
        }
        let max = sums
            .iter()
            .copied()
            .max_by(f64::total_cmp)
            .unwrap_or_default();
        let min = sums
            .iter()
            .copied()
            .min_by(f64::total_cmp)
            .unwrap_or_default();
        let ratio = max / min;
        ratio < 2.0
    }
}

pub struct Stats {
    pub energy: f64,
    pub sums: [f64;4],
    pub means: [f64;4],
    pub rmss: [f64;4],
    pub variances: [f64;4],
    pub snr_sync: SNR,
    pub snr_sync2: SNR,
    pub snr_data: SNR,
    pub snr_data2: SNR,
    pub snr_data_corr: SNR,
    pub snr_data_corr2: SNR,
    pub errors: usize,
    pub errors2: usize
}

impl<T> Deref for Symbols<T> {
    type Target = [T; 162];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Symbols<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[cfg(test)]
mod test {
    use crate::phanol;
    use super::*;

    #[test]
    fn test_encode_decode() {
        const INPUT: u64 = 0b11101010001101111111100100010011011101101001101111;
        let symbols = Symbols::<[f64;4]>::encode(INPUT);
        let mut data = symbols.data_diff();
        data.unshuffle();
        if let Some((_, n)) = phanol::decode(&data) {
            assert_eq!(INPUT, n);
        } else {
            panic!("No decode!")
        }
    }

    // pub const TEST: [u8;162] = [
    //     1,1,0,0,0,0,0,0,1,0,0,0,1,1,1,0,0,0,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,1,0,0,1,0,1,0,0,
    //     0,0,0,0,1,0,1,1,0,0,1,1,0,1,0,0,0,1,1,0,1,0,0,0,0,1,1,0,1,0,1,0,1,0,1,0,0,1,0,0,1,0,
    //     1,1,0,0,0,1,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,1,0,0,1,1,1,0,1,1,0,0,1,1,0,1,0,0,0,1,
    //     1,1,0,0,0,0,0,1,0,1,0,0,1,1,0,0,0,0,0,0,0,1,1,0,1,0,1,1,0,0,0,1,1,0,0,0];

    // #[test]
    // fn test_shuffle() {
    //     let mut xs = TEST.clone();
    //     shuffle(&mut xs);
    //     unshuffle(&mut xs);
    //     assert_eq!(xs, TEST);
    // }
}
