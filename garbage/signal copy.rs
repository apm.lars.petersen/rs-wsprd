use num_complex::Complex;
use rustfft::algorithm::Radix4;
use std::f64::consts::PI;
use std::ops::{Deref, DerefMut, Neg};
use rustfft::*;

#[derive(Debug, Clone)]
pub struct Signal<T = Complex<f64>> {
    pub rate: usize,
    pub samples: Vec<T>,
}

impl <T: Clone> Signal<T> {
    pub fn decimate(&self, factor: usize) -> Self {
        Self {
            samples: self.iter().step_by(factor).cloned().collect(),
            rate: self.rate / factor
        }
    }
}

impl Signal<f32> {
    pub fn load(filename: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let mut reader = hound::WavReader::open(filename)?;
        let spec = reader.spec();
        let rate = spec.sample_rate as usize;
        let mut samples: Vec<f32> = vec![];
        match spec.sample_format {
            hound::SampleFormat::Float => {
                let mut reader = reader.samples::<f32>();
                while let Some(a) = reader.next() {
                    samples.push(a? as f32);
                }
            }
            hound::SampleFormat::Int => {
                let q = 2u32.pow(spec.bits_per_sample as u32 - 1) as f32;
                let mut reader = reader.samples::<i32>();
                while let Some(a) = reader.next() {
                    let x = a? as f32 / q;
                    samples.push(x);
                }
            }
        }

        Ok(Self {
            rate,
            samples,
        })
    }
}

impl Signal<f64> {
    pub fn resize(mut self, len: usize) -> Self {
        self.samples.resize(len, 0.0);
        self
    }

    pub fn segmented_fft(&self) {
        let mut xs: Vec<Complex<f64>> = self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect();
        let fft = Radix4::<f64>::new(8192, FftDirection::Forward);
        let now = std::time::Instant::now();
        let mut out = vec![Complex::new(0.0,0.0); 8192];
        for i in 0..162 {
            //fft.process(&mut xs[i*8192..][..8192]);
            fft.process_outofplace_with_scratch(&mut xs[i*8192..][..8192], &mut out, &mut []);
        }
        println!("ELAPSED FFT: {:?}", now.elapsed());
    }

    pub fn extract_wspr(&self) -> Signal<Complex<f64>> {
        // 2. Make analytical (complex signal) and apply band-pass (1400-1500 Hz)
        let mut s = self.complexify_and_bandpass_padded(1390.0, 1610.0);
        // 3. Normalize amplitude to 80%
        s.normalize(0.8);
        // 4. Shift frequency down by 1312.5 Hz
        let s = s.transpose_hz(-1500.0);
        // 5. Reduce sampling rate to 750 Hz
        s.downsample(32)
    }

    pub fn complexify(&self) -> Signal<Complex<f64>> {
        let mut planner = FftPlanner::<f64>::new();
        let fft = planner.plan_fft_forward(self.len());
        let ifft = planner.plan_fft_inverse(self.len());
        let mut xs: Vec<Complex<f64>> = self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect();
        fft.process(&mut xs);
        for i in xs.len() / 2 .. xs.len() - 1 {
            xs[i] = Complex::new(0.0, 0.0);
        }
        ifft.process(&mut xs);
        Signal {
            rate: self.rate,
            samples: xs,
        }
    }

    pub fn wurp(&self) -> Signal<Complex<f64>> {
        let mut planner = FftPlanner::<f64>::new();
        let fft = planner.plan_fft_forward(self.len());
        let mut xs: Vec<Complex<f64>> = self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect();
        fft.process(&mut xs);
        Signal {
            rate: self.rate,
            samples: xs,
        }
    }

    pub fn complexify_and_bandpass(&self, min: f64, max: f64) -> Signal<Complex<f64>> {
        let mut planner = FftPlanner::<f64>::new();
        let fft = planner.plan_fft_forward(self.len());
        let ifft = planner.plan_fft_inverse(self.len());
        let mut xs: Vec<Complex<f64>> = self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect();
        fft.process(&mut xs);
        // Remove mirror frequencies
        for i in xs.len() / 2 .. xs.len() - 1 {
            xs[i] = Complex::new(0.0, 0.0);
        }
        // Remove lower frequencies
        let n = xs.len();
        for i in 0 .. (min as usize * n / self.rate) {
            xs[i] = Complex::new(0.0, 0.0);
        }
        // Remove higher frequencies
        for i in (max as usize * n / self.rate) .. n / 2 {
            xs[i] = Complex::new(0.0, 0.0);
        }
        ifft.process(&mut xs);
        Signal {
            rate: self.rate,
            samples: xs,
        }
    }

    pub fn complexify_and_bandpass_padded(&self, min: f64, max: f64) -> Signal<Complex<f64>> {
        let mut planner = FftPlanner::<f64>::new();
        let len = self.len();
        let len2 = len * 2;
        let fft = planner.plan_fft_forward(len2);
        let ifft = planner.plan_fft_inverse(len2);
        let mut xs: Vec<Complex<f64>> = self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect();
        xs.resize(len2, Complex::new(0.0,0.0));
        fft.process(&mut xs);
        // Remove mirror frequencies
        for i in len2 / 2 .. len2 - 1 {
            xs[i] = Complex::new(0.0, 0.0);
        }
        // Remove lower frequencies
        for i in 0 .. (min as usize * len2 / self.rate) {
            xs[i] = Complex::new(0.0, 0.0);
        }
        // Remove higher frequencies
        for i in (max as usize * len2 / self.rate) .. len2 / 2 {
            xs[i] = Complex::new(0.0, 0.0);
        }
        ifft.process(&mut xs);
        xs.truncate(len);
        Signal {
            rate: self.rate,
            samples: xs,
        }
    }

    pub fn complex(&self) -> Signal<Complex<f64>> {
        let xs: Vec<Complex<f64>> = self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect();
        Signal {
            rate: self.rate,
            samples: xs,
        }
    }

    pub fn window_blackman_nuttall(mut self) -> Self {
        const A0: f64 = 0.3635819;
        const A1: f64 = 0.4891775;
        const A2: f64 = 0.1365995;
        const A3: f64 = 0.0106411;
        let m1 = (self.len() - 1) as f64;
        let x1 = 2.0 * PI / m1;
        let x2 = 4.0 * PI / m1;
        let x3 = 6.0 * PI / m1;
        for n in 0..self.samples.len() {
            let nf = n as f64;
            let a0 = A0;
            let a1 = A1 * (x1 * nf).cos();
            let a2 = A2 * (x2 * nf).cos();
            let a3 = A3 * (x3 * nf).cos();
            let sf = a0 - a1 + a2 - a3;
            self.samples[n] *= sf;
        }

        self
    }

    pub fn load(filename: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let mut reader = hound::WavReader::open(filename)?;
        let spec = reader.spec();
        let rate = spec.sample_rate as usize;
        let mut samples: Vec<f64> = vec![];
        match spec.sample_format {
            hound::SampleFormat::Float => {
                let mut reader = reader.samples::<f32>();
                while let Some(a) = reader.next() {
                    samples.push(a? as f64);
                }
            }
            hound::SampleFormat::Int => {
                let q = 2u32.pow(spec.bits_per_sample as u32 - 1) as f64;
                let mut reader = reader.samples::<i32>();
                while let Some(a) = reader.next() {
                    let x = a? as f64 / q;
                    samples.push(x);
                }
            }
        }

        Ok(Self {
            rate,
            samples,
        })
    }

    pub fn save(&self, filename: &str) -> Result<(), Box<dyn std::error::Error>> {
        let spec = hound::WavSpec {
            channels: 1,
            sample_rate: self.rate as u32,
            bits_per_sample: 32,
            sample_format: hound::SampleFormat::Float,
        };
        let mut writer = hound::WavWriter::create(filename, spec)?;
        for sample in &self.samples {
            writer.write_sample(*sample as f32)?
        }
        writer.finalize()?;
        Ok(())
    }

    pub fn normalize(&mut self, amplitude: f64) {
        if let Some(max) = self
            .samples
            .iter()
            .max_by(|x, y| f64::total_cmp(&x, &y))
        {
            let factor = amplitude / max;
            self.samples.iter_mut().for_each(|x| *x = *x * factor);
        }
    }
}

impl Signal<Complex<f64>> {
    pub fn new(rate: usize, len: usize) -> Self {
        let x = Complex::new(0.0, 0.0);
        Self {
            rate,
            samples: vec![x; len],
        }
    }

    pub fn new_samples(rate: usize, samples: Vec<Complex<f64>>) -> Self {
        Self {
            rate,
            samples
        }
    }

    pub fn rate(&self) -> usize {
        self.rate
    }

    pub fn new_const(rate: usize, len: usize, val: Complex<f64>) -> Self {
        Self {
            rate,
            samples: vec![val; len],
        }
    }

    pub fn realize(&self) -> Signal<f64> {
        Signal {
            rate: self.rate,
            samples: self.samples.iter().map(|x| x.re).collect(),
        }
    }

    pub fn add(&mut self, other: &Self) {
        self.iter_mut()
            .zip(other.iter())
            .for_each(|(x, y)| *x += *y);
    }

    pub fn add_signal(&mut self, other: &Self) {
        for x in &other.samples {
            self.samples.push(*x)
        }
    }

    pub fn add_silence(&mut self, len: usize) {
        for _ in 0..len {
            self.samples.push(Complex::new(0.0, 0.0))
        }
    }

    pub fn add_offset(&mut self, other: &Self, offset: usize, amp: Complex<f64>) {
        if offset + other.len() > self.len() {
            self.resize(offset + other.len(), Complex::default());
        }
        self.iter_mut()
            .skip(offset)
            .zip(other.iter())
            .for_each(|(x, y)| *x += *y * amp);
    }

    pub fn chirp2(rate: usize, slope: f64, bandwidth: f64) -> Self {
        // 8000:r[samples/s] * 200:[1/s] / 100:b[1/s*s] = 16_0000:n[samples]
        let n: usize = (rate as f64 * bandwidth / slope).ceil() as usize / 2;
        let n21: usize = 2 * n + 1;
        let mut x = vec![Complex::default(); n21];

        let df = 2.0 * PI * slope / (rate as f64 * rate as f64);
        let df = Complex::from_polar(1.0, df);
        let mut dt = Complex::from_polar(1.0, 0.0);
        let mut phi = Complex::new(0.0, 1.0);

        x[n] = phi;

        for i in 1..=n {
            dt *= df;
            dt = dt.unscale(dt.norm());
            phi *= dt;
            phi = phi.unscale(phi.norm());
            x[n + i] = phi;
            x[n - i] = phi;
        }

        Self { rate, samples: x }
    }

    pub fn chirp3(mut self, rate: f64, bandwidth: f64) -> Self {
        // 8000:r[samples/s] * 200:[1/s] / 100:b[1/s*s] = 16_0000:n[samples]
        let n = self.len();
        let slope = 0.0;
        // df: [] 8_000 Hz /
        let df = 2.0 * PI * bandwidth / (n as f64 * rate);
        let df = Complex::from_polar(1.0, df);
        let mut f = Complex::from_polar(1.0, 0.0);
        let mut phi = Complex::new(0.0, 1.0);

        for i in 0..n {
            self.samples[i] = phi;
            f *= df;
            f = f.unscale(f.norm());
            phi *= f;
            phi = phi.unscale(phi.norm());
        }

        self
    }

    pub fn transpose_hz(mut self, df: f64) -> Self {
        let n = self.samples.len();

        let df = Complex::from_polar(1.0, 2.0 * PI * df / self.rate as f64);
        let mut phi = Complex::new(1.0, 0.0);

        for i in 0..n {
            self.samples[i] *= phi;
            phi *= df;
            phi = phi.unscale(phi.norm());
        }

        self
    }

    /// Shift by Hz
    pub fn shift(mut self, df: f64) -> Self {
        let n = self.samples.len();

        let df = Complex::from_polar(1.0, 2.0 * PI * df / self.rate as f64);
        let mut phi = Complex::new(1.0, 0.0);

        for i in 0..n {
            self.samples[i] *= phi;
            phi *= df;
            phi = phi.unscale(phi.norm());
        }

        self
    }

    /// Shift by Hz and drift by Hz/s
    pub fn shift_and_drift(mut self, shift: f64, drift: f64) -> Self {
        let dt = Complex::from_polar(1.0, 2.0 * PI * shift / self.rate as f64);
        let dd = Complex::from_polar(1.0, 2.0 * PI * drift / (self.rate * self.rate) as f64);

        let mut phi = Complex::new(1.0, 0.0);
        let mut psi = Complex::new(1.0, 0.0);
        let mut chi = Complex::new(1.0, 0.0);

        for sample in self.samples.iter_mut() {
            *sample *= phi * chi;
            phi *= dt;
            psi *= dd;
            chi *= psi;
            // Avoid numerical error accumulation
            phi = phi.unscale(phi.norm());
            psi = psi.unscale(psi.norm());
            chi = chi.unscale(chi.norm());
        }

        self
    }

    pub fn transpose_and_drift(&mut self, f_transpose: f64, f_drift: f64) {
        let n = self.samples.len();

        let wspr_samples = self.rate as f64 * (8192.0 / 12000.0) * 162.0;
        let dd = Complex::from_polar(1.0, 2.0 * PI * f_drift / (self.rate as f64 * wspr_samples));
        let dt = Complex::from_polar(1.0, 2.0 * PI * f_transpose / self.rate as f64);

        let mut phi = Complex::new(1.0, 0.0);
        let mut psi = Complex::new(1.0, 0.0);
        let mut chi = Complex::new(1.0, 0.0);

        for i in 0..n {
            self.samples[i] *= phi * chi;
            phi *= dt;
            psi *= dd;
            chi *= psi;
            // Avoid numerical error accumulation
            phi = phi.unscale(phi.norm());
            psi = psi.unscale(psi.norm());
            chi = chi.unscale(chi.norm());
        }
    }

    pub fn downsample(&self, step: usize) -> Self {
        Self {
            samples: self.iter().step_by(step).cloned().collect(),
            rate: self.rate / step
        }
    }

    pub fn conj(&mut self) {
        self.iter_mut().for_each(|x| *x = x.conj())
    }

    pub fn replicate(&mut self, n: usize) {
        let samples = self.samples.clone();
        for _ in 1..n {
            self.samples.extend(&samples);

        }
    }

    pub fn fft(&mut self) {
        let mut planner = FftPlanner::<f64>::new();
        let fft = planner.plan_fft_forward(self.len());
        fft.process(self);
    }

    pub fn ifft(&mut self) {
        let mut planner = FftPlanner::<f64>::new();
        let ifft = planner.plan_fft_inverse(self.len());
        ifft.process(self);
    }

    pub fn amplify2(mut self, factor: f64) -> Self {
        self.samples.iter_mut().for_each(|x| *x = x.scale(factor));
        self
    }

    pub fn amplify(&mut self, factor: f64) {
        self.samples.iter_mut().for_each(|x| *x = x.scale(factor))
    }

    pub fn normalize(&mut self, amplitude: f64) {
        if let Some(max) = self
            .samples
            .iter()
            .map(|x| x.norm())
            .max_by(|x, y| f64::total_cmp(&x, &y))
        {
            let factor = amplitude / max;
            self.samples.iter_mut().for_each(|x| *x = *x * factor);
        }
    }

    pub fn window_raised_cosine(mut self, attack: usize) -> Self {
        let n = self.len();
        for i in 0..attack {
            let f = (i as f64 * PI / attack as f64).cos().neg() / 2.0 + 0.5;
            self.samples[i] *= f;
            self.samples[n - i - 1] *= f;
        }
        self
    }

    pub fn window_blackman_nuttall(mut self) -> Self {
        const A0: f64 = 0.3635819;
        const A1: f64 = 0.4891775;
        const A2: f64 = 0.1365995;
        const A3: f64 = 0.0106411;
        let m1 = (self.len() - 1) as f64;
        let x1 = 2.0 * PI / m1;
        let x2 = 4.0 * PI / m1;
        let x3 = 6.0 * PI / m1;
        for n in 0..self.samples.len() {
            let nf = n as f64;
            let a0 = A0;
            let a1 = A1 * (x1 * nf).cos();
            let a2 = A2 * (x2 * nf).cos();
            let a3 = A3 * (x3 * nf).cos();
            let sf = a0 - a1 + a2 - a3;
            self.samples[n].re *= sf;
            self.samples[n].im *= sf;
        }

        self
    }

    pub fn convolve(&mut self, kernel: &Self) {
        let n = self.samples.len();
        let m = kernel.samples.len();
        assert!(n > m);
        for i in 0..n - m {
            let mut x = Complex::new(0.0, 0.0);
            for j in 0..m {
                x += self.samples[i + j] * kernel[m - j - 1];
            }
            self.samples[i] = x;
        }
    }

    pub fn load(filename: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let mut reader = hound::WavReader::open(filename)?;
        let spec = reader.spec();
        let rate = spec.sample_rate as usize;
        let mut samples: Vec<Complex<f64>> = vec![];
        let mut reader = reader.samples::<f32>();
        while let (Some(a), Some(b)) = (reader.next(), reader.next()) {
            samples.push(Complex::new(a? as f64, b? as f64));
        }
        Ok(Self {
            rate,
            samples,
        })
    }

    pub fn save(&self, filename: &str) -> Result<(), Box<dyn std::error::Error>> {
        let spec = hound::WavSpec {
            channels: 2,
            sample_rate: self.rate as u32,
            bits_per_sample: 32,
            sample_format: hound::SampleFormat::Float,
        };
        let mut writer = hound::WavWriter::create(filename, spec)?;
        for sample in &self.samples {
            writer.write_sample(sample.re as f32)?;
            writer.write_sample(sample.im as f32)?
        }
        writer.finalize()?;
        Ok(())
    }

    pub fn print(&self) {
        for s in &self.samples {
            println!("{}", s.im)
        }
    }
}

impl<T> Deref for Signal<T> {
    type Target = Vec<T>;

    fn deref(&self) -> &Self::Target {
        &self.samples
    }
}

impl<T> DerefMut for Signal<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.samples
    }
}
