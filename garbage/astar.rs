use crate::constants::{POLY1, POLY1_REV, POLY2, POLY2_REV};
use crate::ite;
use crate::symbols::Symbols;
use std::collections::{BinaryHeap, HashMap};

const ITER_POW: usize = 26;
const ITER_MAX: usize = 1 << ITER_POW;

const QUEUE_POW: usize = 22;
const QUEUE_LEN: usize = 1 << QUEUE_POW;

pub struct Astar;

impl Astar {
    pub fn search(ss: &Symbols<f64>) -> Vec<SearchResult> {
        let avg = ss.iter().sum::<f64>() as f32 / 162.0;
        let xs = ss.0.map(|x| x as f32);
        let ys = xs.map(|x| x - avg);
        let xs = Self::deinterleave(&xs);
        let ys = Self::deinterleave(&ys);
        vec![
            // Self::search_forward(&xs).named("FWD"),
            // Self::search_forward(&ys).named("FWD_NODC"),
            // Self::search_backward(&xs).named("BWD"),
            // Self::search_backward(&ys).named("BWD_NODC"),
            Self::search_both(&xs).named("FB"),
            Self::search_both(&ys).named("FB_NODC"),
        ]
    }

    fn search_forward(xs: &[(f32, f32); 81]) -> PartialResult{
        let mut h = BinaryHeap::<Node>::with_capacity(QUEUE_LEN);
        let mut i = 0;
        let mut m = None;

        h.push(Node::default());
        let t = std::time::Instant::now();

        while i < ITER_MAX {
            if h.len() == QUEUE_LEN {
                let mut q = std::mem::take(&mut h).into_vec();
                q.sort_by(|a, b| b.mtr.total_cmp(&a.mtr));
                q.truncate(QUEUE_LEN / 2);
                h = q.into();
            }

            if let Some(mut n) = h.pop() {
                let idx = (n.res & 0xff) as usize;
                if let Some((a, b)) = xs.get(idx) {
                    n.srs <<= 1;
                    n.res += 1;
                    if n.res != 8 {
                        let mut n0: Node = n;
                        n0.mtr += Self::err(n0.srs, *a, *b);
                        h.push(n0);
                    }
                    if idx < 50 {
                        let mut n1: Node = n;
                        n1.srs |= 1;
                        n1.res |= 1 << (idx + 8);
                        n1.mtr += Self::err(n1.srs, *a, *b);
                        h.push(n1);
                    }
                } else {
                    m = Some(n);
                    break;
                }
            }

            i += 1;
        }

        let n = m.or_else(|| h.pop()).unwrap_or_default();
        PartialResult {
            iterations: i,
            attempts: 0,
            progress: (n.res & 0xFF) as usize,
            duration: t.elapsed(),
            data: n.res >> 8,
            likelyhood: Self::likelyhood(xs, n.res),
        }
    }

    fn search_backward(xs: &[(f32, f32); 81]) -> PartialResult{
        let mut ys = *xs;
        ys.reverse();
        let mut h = BinaryHeap::<Node>::with_capacity(QUEUE_LEN);
        let mut i = 0;
        let mut m = None;

        h.push(Node::default());
        let t = std::time::Instant::now();

        while i < ITER_MAX {
            if h.len() == QUEUE_LEN {
                let mut q = std::mem::take(&mut h).into_vec();
                q.sort_by(|a, b| b.mtr.total_cmp(&a.mtr));
                q.truncate(QUEUE_LEN / 2);
                h = q.into();
            }

            if let Some(mut n) = h.pop() {
                let idx = (n.res & 0xff) as usize;
                if let Some((a, b)) = ys.get(idx) {
                    n.srs <<= 1;
                    n.res += 1;
                    if n.res != 8 {
                        let mut n0: Node = n;
                        n0.mtr += Self::err_rev(n0.srs, *a, *b);
                        h.push(n0);
                    }
                    if idx < 50 {
                        let mut n1: Node = n;
                        n1.srs |= 1;
                        n1.res |= 1 << (idx + 8);
                        n1.mtr += Self::err_rev(n1.srs, *a, *b);
                        h.push(n1);
                    }
                } else {
                    m = Some(n);
                    break;
                }
            }

            i += 1;
        }

        let n = m.or_else(|| h.pop()).unwrap_or_default();
        let d = (n.res >> 8).reverse_bits() >> 14;
        let p = (n.res & 0xFF) as usize;
        PartialResult {
            iterations: i,
            attempts: 0,
            progress: p,
            duration: t.elapsed(),
            data: d,
            likelyhood: Self::likelyhood(xs, d << 8 | p as u64),
        }
    }

    fn search_both(xs: &[(f32, f32); 81]) -> PartialResult{
        let xf = *xs;
        let xb = { let mut z= *xs; z.reverse(); z };
        let mut hf = BinaryHeap::<Node>::with_capacity(QUEUE_LEN);
        let mut hb = BinaryHeap::<Node>::with_capacity(QUEUE_LEN);
        let mut sf = HashMap::<u32,u64>::new();
        let mut sb = HashMap::<u32,u64>::new();
        let mut i = 0;
        let mut d = 0;
        let mut o = 0;

        hf.push(Node::default());
        hb.push(Node::default());
        let t = std::time::Instant::now();

        while i < ITER_MAX {
            let (x, h0, s0, s1, poly1, poly2) = if sf.len() < sb.len() {
                (&xf, &mut hf, &mut sf, &mut sb, POLY1, POLY2)
            } else {
                (&xb, &mut hb, &mut sb, &mut sf, POLY1_REV, POLY2_REV)
            };

            if h0.len() == QUEUE_LEN {
                let mut q = std::mem::take(h0).into_vec();
                q.sort_by(|a, b| b.mtr.total_cmp(&a.mtr));
                q.truncate(QUEUE_LEN / 2);
                *h0 = q.into();
            }

            if let Some(mut n) = h0.pop() {
                let idx = (n.res & 0xff) as usize;
                if idx == 41 {
                    if let Some(zzz) = s1.get(&n.srs) {
                        o = s0.len();
                        d = if poly1 == POLY1 {
                            (zzz.reverse_bits() >> 14) | (n.res >> 8)
                        } else {
                            ((n.res >> 8).reverse_bits() >> 14) | zzz
                        };
                        break;
                    } else {
                        s0.insert(n.srs.reverse_bits(), n.res >> 8);
                    }
                } else if n.res != 8{
                    let (a,b) = x[idx];
                    n.srs <<= 1;
                    n.res += 1;
                    let mut n0: Node = n;
                    let mut n1: Node = n;
                    n1.srs |= 1;
                    n1.res |= 1 << (idx + 8);
                    n0.mtr += Self::err_fb(n0.srs, poly1, poly2, a, b);
                    n1.mtr += Self::err_fb(n1.srs, poly1, poly2, a, b);
                    h0.push(n0);
                    h0.push(n1);
                }
            }

            i += 1;
        }

        PartialResult {
            iterations: i,
            attempts: o,
            progress: 23,
            duration: t.elapsed(),
            data: d,
            likelyhood: Self::likelyhood(xs, (d << 8) | 81),
        }
    }

    pub fn likelyhood(xs: &[(f32, f32); 81], res: u64) -> f64 {
        if res & 0xFF < 81 {
            return 0.0;
        }
        let mut dat = res >> 8;
        let mut srs = 0u32;
        let mut max = 0.0;
        let mut err = 0.0;
        for (a,b) in xs {
            srs <<= 1;
            srs |= dat as u32 & 1;
            max += a.abs() + b.abs();
            err += Self::err(srs, *a, *b);
            dat >>= 1;
        }
        ((max + err) / max) as f64
    }

    fn deinterleave(xs: &[f32; 162]) -> [(f32,f32);81] {
        let mut ys = [(0f32, 0f32); 81];
        ys.iter_mut()
            .enumerate()
            .for_each(|(i, x)| *x = (xs[i * 2], xs[i * 2 + 1]));
        ys
    }

    #[inline]
    fn err(srs: u32, a: f32, b: f32) -> f32 {
        let f = |x: u32| ite(x.count_ones() & 1 == 0, 1.0, -1.0);
        (a * f(srs & POLY1)).min(0.0) + (b * f(srs & POLY2)).min(0.0)
    }

    #[inline]
    fn err_fb(srs: u32, poly1: u32, poly2: u32, a: f32, b: f32) -> f32 {
        let f = |x: u32| ite(x.count_ones() & 1 == 0, 1.0, -1.0);
        (a * f(srs & poly1)).min(0.0) + (b * f(srs & poly2)).min(0.0)
    }

    #[inline]
    fn err_rev(srs: u32, a: f32, b: f32) -> f32 {
        let f = |x: u32| ite(x.count_ones() & 1 == 0, 1.0, -1.0);
        (a * f(srs & POLY1_REV)).min(0.0) + (b * f(srs & POLY2_REV)).min(0.0)
    }
}

pub struct SearchResult {
    pub name: String,
    pub iterations: usize,
    pub progress: usize,
    pub duration: std::time::Duration,
    pub data: u64,
    pub likelyhood: f64,
    pub attempts: usize,
}

pub struct PartialResult {
    pub iterations: usize,
    pub progress: usize,
    pub duration: std::time::Duration,
    pub data: u64,
    pub likelyhood: f64,
    pub attempts: usize,
}

impl PartialResult {
    pub fn named(self, name: &str) -> SearchResult {
        SearchResult {
            name: name.to_string(),
            data: self.data,
            progress: self.progress,
            iterations: self.iterations,
            likelyhood: self.likelyhood,
            duration: self.duration,
            attempts: self.attempts,
        }
    }
}

#[derive(Debug, Default, Clone, Copy)]
struct Node {
    mtr: f32,
    srs: u32,
    res: u64,
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.mtr == other.mtr
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.mtr.partial_cmp(&other.mtr)
    }
}

impl Eq for Node {}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.mtr.total_cmp(&other.mtr)
    }
}
