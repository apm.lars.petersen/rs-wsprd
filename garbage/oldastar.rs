use crate::constants::{POLY1, POLY2};
use crate::ite;
use crate::snr::SNR;
use crate::symbols::Symbols;
use std::collections::BinaryHeap;

const ITER_POW: usize = 25;
const ITER_MAX: usize = 1 << ITER_POW;

const QUEUE_POW: usize = 25;
const QUEUE_LEN: usize = 1 << QUEUE_POW;

pub struct Decode {
    pub name: String,

    pub snr: SNR,
    pub data: u64,
    pub finished: bool,
    pub iterations: usize,
    pub duration: std::time::Duration,
}

impl Decode {
    pub fn new_astar3_ng(os: &Symbols<[f64; 2]>) -> Self {
        let xx: [(f64, f64); 162] =
            os.0.map(|x| ((x[1] - x[0]).max(0.0), (x[0] - x[1]).max(0.0)));
        let diff = xx.map(|x| (x.0 - x.1));
        Self::new_astar_ng(&diff).name("ASTAR3")
    }

    pub fn new_astar4_ng(os: &Symbols<[f64; 2]>) -> Self {
        let xx: [(f64, f64); 162] =
            os.0.map(|x| ((x[1] - x[0]).max(0.0), (x[0] - x[1]).max(0.0)));
        let diff = xx.map(|x| (x.0 - x.1));
        let sum = diff.iter().sum::<f64>();
        let diff = diff.map(|x| x - sum / 162.);
        Self::new_astar_ng(&diff).name("ASTAR4")
    }

    pub fn new_astar_ng(diff: &[f64; 162]) -> Self {
        let max: f64 = diff.iter().map(|x| x.abs()).sum();

        let mut xxx = [(0f64, 0f64, 0f64); 81];
        let mut acc = 0.0;
        for i in (0..81).rev() {
            let a = diff[i * 2];
            let b = diff[i * 2 + 1];
            xxx[i] = (a, b, acc);
            acc += a.abs() + b.abs();
        }

        let f0 = |n0: &mut Node| {
            let (a, b, opt) = xxx[n0.idx];

            n0.acc += a * par(n0.srs & POLY1) + b * par(n0.srs & POLY2);
            n0.mtr = n0.acc + opt;
        };

        let now = std::time::Instant::now();
        let (it, n) = Self::search(f0);
        let elapsed = now.elapsed();

        let signal = n.acc;
        let noise = max - signal;

        return Self {
            name: "".to_string(),
            snr: SNR { signal, noise },
            data: n.res,
            finished: true,
            iterations: it,
            duration: elapsed,
        };
    }

    fn name(mut self, name: &str) -> Self {
        self.name = name.to_string();
        self
    }

    fn search<F: Fn(&mut Node)>(f: F) -> (usize, Node) {
        let mut h = BinaryHeap::<Node>::with_capacity(QUEUE_LEN);
        h.push(Node::default());

        for i in 0..ITER_MAX {
            if h.len() == QUEUE_LEN {
                let t = std::mem::take(&mut h);
                let mut t = t.into_vec();
                t.sort_by(|a, b| b.mtr.total_cmp(&a.mtr));
                t.truncate(QUEUE_LEN / 2);
                h = t.into();
            }

            if let Some(mut n) = h.pop() {
                if n.idx < 50 {
                    n.srs <<= 1;
                    let mut n0: Node = n;
                    let mut n1: Node = n;
                    n1.srs |= 1;
                    n1.res |= 1 << n1.idx;
                    f(&mut n0);
                    f(&mut n1);
                    n0.idx += 1;
                    n1.idx += 1;
                    h.push(n0);
                    h.push(n1);
                } else if n.idx < 81 {
                    n.srs <<= 1;
                    f(&mut n);
                    n.idx += 1;
                    h.push(n);
                } else {
                    return (i, n);
                }
            }
        }

        (ITER_MAX, h.pop().unwrap())
    }

    pub fn new_astar32(os: &Symbols<[f64; 2]>) -> Self {
        let xx: [(f32, f32); 162] =
            os.0.map(|x| ((x[1] - x[0]).max(0.0) as f32, (x[0] - x[1]).max(0.0) as f32));
        let diff = xx.map(|x| (x.1 - x.0));
        Self::new_astar_2(&diff).name("ASTAR32")
    }

    pub fn new_astar42(os: &Symbols<[f64; 2]>) -> Self {
        let xx: [(f32, f32); 162] =
            os.0.map(|x| ((x[1] - x[0]).max(0.0) as f32, (x[0] - x[1]).max(0.0) as f32));
        let diff = xx.map(|x| (x.1 - x.0));
        let sum = diff.iter().sum::<f32>();
        let diff = diff.map(|x| x - sum / 162.);
        Self::new_astar_2(&diff).name("ASTAR42")
    }

    pub fn new_astar_2(diff: &[f32; 162]) -> Self {
        let mut xxx = [(0f32, 0f32); 81];
        for i in 0..81 {
            let a = diff[i * 2];
            let b = diff[i * 2 + 1];
            xxx[i] = (a, b);
        }

        let now = std::time::Instant::now();
        let (it, n) = Self::search2(&xxx);
        let elapsed = now.elapsed();

        let signal = 1.0;
        let noise = 1.0;

        return Self {
            name: "".to_string(),
            snr: SNR { signal, noise },
            data: n.res >> 8,
            finished: true,
            iterations: it,
            duration: elapsed,
        };
    }

    fn search2(xxx: &[(f32, f32); 81]) -> (usize, Node2) {
        let mut h = BinaryHeap::<Node2>::with_capacity(QUEUE_LEN);
        h.push(Node2::default());

        for i in 0..ITER_MAX {
            if h.len() == QUEUE_LEN {
                let t = std::mem::take(&mut h);
                let mut t = t.into_vec();
                t.sort_by(|a, b| b.mtr.total_cmp(&a.mtr));
                t.truncate(QUEUE_LEN / 2);
                h = t.into();
            }

            if let Some(mut n) = h.pop() {
                let idx = (n.res & 0xff) as usize;
                if let Some((a, b)) = xxx.get(idx) {
                    let p = |x: u32| ite(x.count_ones() & 1 == 0, 1.0, -1.0);
                    let f = |srs: u32| (a * p(srs & POLY1)).min(0.0) + (b * p(srs & POLY2)).min(0.0);

                    if idx < 50 {
                        n.srs <<= 1;
                        n.res += 1;
                        let mut n0: Node2 = n;
                        let mut n1: Node2 = n;
                        n1.srs |= 1;
                        n1.res |= 1 << (idx + 8);
                        n0.mtr += f(n0.srs);
                        n1.mtr += f(n1.srs);
                        h.push(n0);
                        h.push(n1);
                    } else {
                        n.srs <<= 1;
                        n.res += 1;
                        n.mtr += f(n.srs);
                        h.push(n);
                    }
                } else {
                    return (i, n);
                }
            }
        }

        (ITER_MAX, h.pop().unwrap())
    }
}

#[derive(Debug, Default, Clone, Copy)]
struct Node {
    mtr: f64,
    acc: f64,
    idx: usize, // Index [0..80]
    srs: u32,   // Shift register state
    res: u64,   // Result bits whereas bit 0 = LSB, bit 49 at << 49
}

#[inline(always)]
fn par(x: u32) -> f64 {
    if x.count_ones() & 1 == 0 {
        -1.0
    } else {
        1.0
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.mtr == other.mtr
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.mtr.partial_cmp(&other.mtr)
    }
}

impl Eq for Node {}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.mtr.total_cmp(&other.mtr)
    }
}

#[derive(Debug, Default, Clone, Copy)]
struct Node2 {
    mtr: f32,
    srs: u32,
    res: u64,
}

impl PartialEq for Node2 {
    fn eq(&self, other: &Self) -> bool {
        self.mtr == other.mtr
    }
}

impl PartialOrd for Node2 {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.mtr.partial_cmp(&other.mtr)
    }
}

impl Eq for Node2 {}

impl Ord for Node2 {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.mtr.total_cmp(&other.mtr)
    }
}
