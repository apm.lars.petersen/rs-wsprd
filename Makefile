.PHONY: wsprd

wsprd:
	cargo run --release --bin wsprd samples/wspr_14260087.12kHz.wav

refine:
	cargo run --release --bin wsprd-refine call-phase.wav

refine-x:
	cargo run --release --bin wsprd-refine samples/wspr_14260087.12kHz.wav

refine-y:
	cargo run --release --bin wsprd-refine blocks/wspr_14259351.wav

nophase:
	cargo run --release --bin wsprd call.wav

phase:
	cargo run --release --bin wsprd call-phase.wav

bandwidth:
	cargo run --release --bin wsprd-bandwidth

wsprd-orig:
	(cd wsprd; wsprd -d -s ../samples/wspr_14260087.16bit.wav)

wsprd-enc:
	cargo run --release --bin wsprd-encode