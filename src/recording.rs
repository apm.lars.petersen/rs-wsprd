use crate::qdx;

pub struct WisprChunker {
    block: usize,
    synced: bool,
    buffer: Vec<f32>
}

impl WisprChunker {
    pub const SECONDS: usize = 114;
    pub const INPUT_RATE: usize = 48_000;
    pub const INPUT_CHANNELS: usize = 2;
    pub const OUTPUT_RATE: usize = 12_000;
    pub const OUTPUT_CHANNELS: usize = 1;

    pub fn samples() -> usize {
        Self::SECONDS * Self::INPUT_RATE * Self::INPUT_CHANNELS
    }

    pub fn downsampling_factor() -> usize {
        (Self::INPUT_RATE / Self::OUTPUT_RATE) * (Self::INPUT_CHANNELS / Self::OUTPUT_CHANNELS)
    }

    pub fn new() -> Self {
        let buf = Vec::with_capacity(Self::samples());

        Self {
            block: 0,
            synced: false,
            buffer: buf,
        }
    }

    pub fn save(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        println!("Saving block {}", self.block);
        let filename = format!("blocks/wspr_{}.wav", self.block);
        let spec = hound::WavSpec {
            channels: Self::OUTPUT_CHANNELS as u16,
            sample_rate: (Self::OUTPUT_RATE) as u32,
            bits_per_sample: 32,
            sample_format: hound::SampleFormat::Float,
        };
        let mut writer = hound::WavWriter::create(filename, spec)?;
        let factor = Self::downsampling_factor();
        for i in 0..self.buffer.len()/factor {
            let sample = self.buffer[i*factor];
            writer.write_sample(sample)?;
        }
        writer.finalize()?;
        Ok(())
    }

    pub fn synced(&mut self) -> bool {
        if !self.synced {
            let epoch = std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap();
            let block_old = self.block;
            let block_new = (epoch.as_millis() / 1000 / 120) as usize;
            self.block = block_new;
            if block_new > block_old && block_old > 0 {
                println!("Synced to block {}", self.block);
                self.synced = true;
            }
        }
        self.synced
    }
}

impl qdx::QdxConsumer for WisprChunker {
    fn on_data(&mut self, data: &[f32]) -> Result<(), Box<dyn std::error::Error>> {
        if self.synced() {
            self.buffer.extend(data);
        }
        if self.buffer.len() >= Self::samples() {
            self.buffer.truncate(Self::samples());
            self.save()?;
            self.buffer.truncate(0);
            self.synced = false;
        }
        Ok(())
    }

    fn on_error(&mut self, err: String) {
        eprintln!("{:?}", err);
    }
}
