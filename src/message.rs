mod callsign;
mod locator;
mod power;
mod util;

pub use self::callsign::*;
pub use self::locator::*;
pub use self::power::*;

#[derive(Debug, Clone, Copy)]
pub struct Message {
    pub callsign: Callsign,
    pub locator: Locator,
    pub power: Power,
    pub typ: usize
}

impl Message {
    pub fn encode(&self) -> u64 {
        let n1 = self.callsign.0 << 22;
        let n2 = self.locator.0 << 7;
        let n3 = self.power.0 + 64;
        n1 + n2 + n3
    }

    pub fn decode(mut n: u64) -> Option<Self> {
        let mut q = 0u64;
        for _ in 0..50 {
            q <<= 1;
            q |= n & 1;
            n >>= 1;
        }

        let power = q & 63;

        if q & 64 != 0 {
            if power % 10 == 0 || power % 10 == 3 || power % 10 == 7 {
                Some(Self {
                    typ: 1,
                    callsign: Callsign(q >> 22),
                    locator: Locator ((q >> 7) & 32767),
                    power: Power(power),
                })
            } else {
                Some(Self {
                    typ: 2,
                    callsign: Callsign(q >> 22),
                    locator: Locator ((q >> 7) & 32767),
                    power: Power(power),
                })
            }
        } else {
            Some(Self {
                typ: 3,
                callsign: Callsign(q >> 22),
                locator: Locator ((q >> 7) & 32767),
                power: Power(power),
            })
        }
    }
}

impl std::fmt::Display for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{: >8} {: >6} {: >3}", self.callsign.to_string(), self.locator.to_string(), self.power.to_string())
    }
}
