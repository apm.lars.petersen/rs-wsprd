#![feature(array_windows)]
pub mod qdx;
pub mod recording;
pub mod signal;
pub mod message;
pub mod constants;
pub mod fano;
pub mod symbols;
pub mod plot;
pub mod chistar;
pub mod snr;
pub mod lrr;
pub mod rate;
pub mod spectrum;
pub mod list;
pub mod refine;
pub mod location;

macro_rules! ite {
    ($i:expr, $a:expr, $b:expr) => {
        if $i {
            $a
        } else {
            $b
        }
    };
}

macro_rules! flip {
    ($i:expr, $a:expr, $b:expr) => {
        if $i {
            ($a, $b)
        } else {
            ($b, $a)
        }
    };
}

pub(crate) use ite;
pub(crate) use flip;
