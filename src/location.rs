#[derive(Debug, Clone, Copy, Default)]
pub struct Location {
    pub freq: f64,
    pub time: f64,
    pub drift: f64,
}
