use std::{ops::AddAssign, str::FromStr};

use num_complex::Complex64;

#[derive(Debug, Clone, Copy)]
pub struct LRR {
    pub e0: f64,
    pub e1: f64,
    pub e2: f64,
    pub e3: f64,
}

impl LRR {
    pub const fn new(ratio: f64) -> Self {
        Self {
            e0: ratio,
            e1: 1.,
            e2: 1.,
            e3: 1.,
        }
    }

    pub const fn zero() -> Self {
        Self {
            e0: 0.0,
            e1: 0.0,
            e2: 0.0,
            e3: 0.0,
        }
    }

    /// Compute the left-right-ratio
    ///
    /// Rule of thumb: The higher the LRR the worse the signal.
    ///
    /// The LRR is an indicator for being 2 bins off the actual signal.
    /// In this case the correlation with the sync vector is very high although
    /// it is not a desired match as half the information is missing.
    ///
    /// The LRR is computed as `max(e01/e23, e23/e01)` with
    /// `eXY` as the enery sum of both the left resp. right tone.
    pub fn new_complex(s0: Complex64, s1: Complex64, s2: Complex64, s3: Complex64,) -> Self {
        Self {
            e0: s0.norm_sqr(),
            e1: s1.norm_sqr(),
            e2: s2.norm_sqr(),
            e3: s3.norm_sqr(),
        }
    }

    pub fn ratio(&self) -> f64 {
        self.e0.max(self.e1).max(self.e2).max(self.e3) / self.e0.min(self.e1).min(self.e2).min(self.e3)
    }
}

impl Default for LRR {
    fn default() -> Self {
        Self::zero()
    }
}

impl AddAssign for LRR {
    fn add_assign(&mut self, other: Self) {
        self.e0 += other.e0;
        self.e1 += other.e1;
        self.e2 += other.e2;
        self.e3 += other.e3;
    }
}

impl std::fmt::Display for LRR {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.ratio().fmt(f)
    }
}

impl FromStr for LRR {
    type Err = <f64 as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let ratio: f64 = s.parse()?;
        Ok(Self::new(ratio))
    }
}

impl PartialEq for LRR {
    fn eq(&self, other: &Self) -> bool {
        self.ratio() == other.ratio()
    }
}

impl PartialOrd for LRR {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.ratio().partial_cmp(&other.ratio())
    }
}
