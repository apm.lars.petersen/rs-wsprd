use crate::constants::{SYMBOL_DISTANCE, SYNC};
use core::f64;
use num_complex::Complex64;
use std::ops::{Add, AddAssign, Mul};
use std::str::FromStr;

#[derive(Debug, Clone, Copy)]
pub struct SNR {
    pub signal: f64,
    pub noise: f64,
}

impl SNR {
    pub const fn new(ratio: f64) -> Self {
        Self {
            signal: ratio,
            noise: 1.0,
        }
    }

    pub const fn new_sn(signal: f64, noise: f64) -> Self {
        Self { signal, noise }
    }

    pub fn with_sync(symbol: &[Complex64; 4], idx: usize) -> SNR {
        let e0 = symbol[0].norm_sqr();
        let e1 = symbol[1].norm_sqr();
        let e2 = symbol[2].norm_sqr();
        let e3 = symbol[3].norm_sqr();
        let e02 = e0 + e2;
        let e13 = e1 + e3;
        let (signal, noise) = match SYNC[idx] {
            0 => (e02 - e13, e13 + e13),
            _ => (e13 - e02, e02 + e02),
            // 0 => (e02, e13),
            // _ => (e13, e02),
        };
        SNR { signal, noise }
    }

    pub fn with_sync_weighted(symbol: &[Complex64; 4], idx: usize) -> SNR {
        let e0 = symbol[0].norm_sqr();
        let e1 = symbol[1].norm_sqr();
        let e2 = symbol[2].norm_sqr();
        let e3 = symbol[3].norm_sqr();
        let e02 = e0 + e2;
        let e13 = e1 + e3;
        let (signal, noise) = match SYNC[idx] {
            0 => (e02 - e13, e13 + e13),
            _ => (e13 - e02, e02 + e02),
        };
        SNR { signal, noise }
    }

    pub fn with_syncf(symbol: &[f64; 4], idx: usize) -> SNR {
        let e0 = symbol[0];
        let e1 = symbol[1];
        let e2 = symbol[2];
        let e3 = symbol[3];
        let e02 = e0 + e2;
        let e13 = e1 + e3;
        let (signal, noise) = match SYNC[idx] {
            0 => (e02 - e13, e13 + e13),
            _ => (e13 - e02, e02 + e02),
        };
        SNR { signal, noise }
    }

    pub fn with_data(symbol: &[Complex64; 4], data: u8) -> SNR {
        let (a, b, c, d) = match data {
            0 => (0, 1, 2, 3),
            1 => (1, 0, 2, 3),
            2 => (2, 0, 1, 3),
            _ => (3, 0, 1, 2),
        };
        let ea = symbol[a].norm_sqr();
        let eb = symbol[b].norm_sqr();
        let ec = symbol[c].norm_sqr();
        let ed = symbol[d].norm_sqr();
        let avg_noise = (eb + ec + ed) / 3.0;
        let est_signal = ea - avg_noise;
        SNR {
            signal: est_signal,
            noise: avg_noise * 4.0,
        }
    }

    pub fn zero() -> Self {
        Self {
            signal: 0.0,
            noise: 0.0,
        }
    }

    pub fn ratio(&self) -> f64 {
        self.signal / self.noise
    }

    pub fn percent(&self) -> f64 {
        100. * self.signal / (self.signal + self.noise)
    }

    pub fn db(self) -> f64 {
        10.0 * self.ratio().log10()
    }

    pub fn db_2k5(self) -> f64 {
        let mut x = self;
        x.noise *= 2500. / SYMBOL_DISTANCE;
        10.0 * x.ratio().log10()
    }
}

impl Default for SNR {
    fn default() -> Self {
        Self {
            signal: 0.0,
            noise: f64::INFINITY,
        }
    }
}

impl Add for SNR {
    type Output = Self;

    fn add(mut self, other: Self) -> Self {
        self.signal += other.signal;
        self.noise += other.noise;
        self
    }
}

impl AddAssign for SNR {
    fn add_assign(&mut self, other: Self) {
        self.signal += other.signal;
        self.noise += other.noise;
    }
}

impl Mul<f64> for SNR {
    type Output = Self;

    fn mul(mut self, other: f64) -> Self {
        self.signal *= other;
        self.noise *= other;
        self
    }
}

impl std::fmt::Display for SNR {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.ratio().fmt(f)
    }
}

impl FromStr for SNR {
    type Err = <f64 as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let ratio: f64 = s.parse()?;
        Ok(Self::new(ratio))
    }
}

impl PartialEq for SNR {
    fn eq(&self, other: &Self) -> bool {
        self.ratio() == other.ratio()
    }
}

impl PartialOrd for SNR {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.ratio().partial_cmp(&other.ratio())
    }
}

impl Eq for SNR {}

impl Ord for SNR {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.ratio().total_cmp(&other.ratio())
    }
}
