use std::collections::HashSet;

use colored::Colorize;

use crate::message::{Callsign, Locator, Power};



pub struct List(HashSet<String>);

impl List {
    pub fn load() -> Result<Self, Box<dyn std::error::Error>> {
        let mut s = HashSet::new();
        let x = include_str!("../samples/wspr_14260087.csv");
        for line in x.lines() {
            if let Some(call) = line.split(',').skip(7).next() {
                let call: String = call.chars().filter(|c| c != &'"').collect();
                let len = call.len();
                for i in 1..=len {
                    s.insert(call[0..i].to_string());
                }
            }
        }

        Ok(Self(s))
    }

    pub fn contains(&self, call: &str) -> bool {
        self.0.contains(call)
    }

    /// Look what prefix of the given callsign is contained in the list and return it.
    pub fn color(&self, call: Callsign, locator: Locator, power: Power) -> String {
        let call = call.to_string();
        let len = call.len();

        for i in 0..len {
            let prefix = &call[0..len-i];
            if self.0.contains(prefix) {
                return format!("{}{} {} {}", prefix.green(), call.strip_prefix(prefix).unwrap(), locator, power);
            }
        }
        format!("{} {} {}", call, locator, power)
    }
}
