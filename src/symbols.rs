use crate::constants::{OUTLIER_XSIGMA, POLY1, POLY2, SYMBOL_COUNT, SYNC, SYNC64};
use crate::ite;
use crate::snr::SNR;
use num_complex::Complex64;
use rustfft::num_traits::Zero;
use std::ops::{Deref, Neg};
use std::ops::DerefMut;

#[derive(Debug, Copy, Clone)]
pub struct Symbols<T = [Complex64; 4]>(pub [T; SYMBOL_COUNT]);

impl<T: Default + Clone + Copy> Symbols<T> {
    pub fn new() -> Self {
        Self([Default::default(); SYMBOL_COUNT])
    }

    pub fn shuffle(&mut self) {
        let mut tmp = [T::default(); SYMBOL_COUNT];
        let mut p: usize = 0;
        let mut i: u8 = 0;
        let mut j: u8;

        while p < SYMBOL_COUNT {
            j = i.reverse_bits();
            if j < SYMBOL_COUNT as u8 {
                tmp[j as usize] = self[p];
                p = p + 1;
            }
            i = i + 1;
        }
        for i in 0..SYMBOL_COUNT {
            self[i] = tmp[i];
        }
    }

    pub fn unshuffle(mut self) -> Self {
        let mut tmp = [T::default(); SYMBOL_COUNT];
        let mut p: usize = 0;
        let mut i: u8 = 0;
        let mut j: u8;

        while p < SYMBOL_COUNT {
            j = i.reverse_bits();
            if j < SYMBOL_COUNT as u8 {
                tmp[p] = self[j as usize];
                p = p + 1;
            }
            i = i + 1;
        }
        for i in 0..SYMBOL_COUNT {
            self[i] = tmp[i];
        }

        self
    }
}

impl Symbols<u8> {
    // FIXME: Separate Sync vector merge
    pub fn encode(n: u64) -> Self {
        let mut x = Symbols([0u8; SYMBOL_COUNT]);
        let mut n = n;
        let mut s = 0u32;

        for i in 0..81 {
            s <<= 1;
            s |= n as u32 & 1;
            n >>= 1;
            x[i * 2 + 0] = (s & POLY1).count_ones() as u8 % 2;
            x[i * 2 + 1] = (s & POLY2).count_ones() as u8 % 2;
        }

        x.shuffle();

        for i in 0..SYMBOL_COUNT {
            x[i] = 2 * x[i] + SYNC[i] as u8;
        }

        x
    }
}

impl Symbols<f64> {
    pub fn encode(n: u64) -> Self {
        let mut x = Symbols([0f64; SYMBOL_COUNT]);
        let mut n = n;
        let mut s = 0u32;

        let f = |bit| ite!(bit == 0, -1.0, 1.0);

        for i in 0..81 {
            s <<= 1;
            s |= n as u32 & 1;
            n >>= 1;
            x[i * 2 + 0] = f((s & POLY1).count_ones() as u8 % 2);
            x[i * 2 + 1] = f((s & POLY2).count_ones() as u8 % 2);
        }

        x.shuffle();
        x
    }

    pub fn bits(&self) -> [u8; SYMBOL_COUNT] {
        let mut x = [0; SYMBOL_COUNT];
        for i in 0..SYMBOL_COUNT {
            x[i] = if self[i] > 0.0 { 1 } else { 0 }
        }
        x
    }

    pub fn abs_avg(&self) -> f64 {
        self.iter().map(|x|x.abs()).sum::<f64>() / 162.0
    }

    pub fn abs_std(&self) -> f64 {
        let avg = self.abs_avg();
        let sum = self.iter().map(|x| (x - avg) * (x - avg)).sum::<f64>();
        (sum / 162.0).sqrt()
    }

    pub fn outliers(&self) -> Symbols<bool> {
        let avg = self.abs_avg();
        let std = self.abs_std();
        let threshold = avg + std;
        Symbols(self.map(|x| x.abs() > threshold))
    }

    pub fn print_outliers(&self) {
        const FACTOR: f64 = 5.0;
        let mut x = self.0.map(|x|x.abs());
        for i in 0..162 {
            let outlier = match i {
                0 => x[i] > FACTOR * x[i+1],
                161 => x[i] > FACTOR * x[i-1],
                _ => x[i] > FACTOR * (x[i+1] + x[i-1]) / 2.0
            };
            print!("\t{:10.5}", x[i] * 100_000.);
            if outlier {
                print!(" <--");
            }
            if x[i] > self.abs_avg() + self.abs_std() {
                print!(" <==");
            }
            println!();
        }
        println!("\t{:10.5} SIGMA", self.abs_std() * 100_000.);
    }

    pub fn sqrt(self) -> Self {
        Self(self.0.map(|x| x.signum() * x.abs().sqrt()))
    }

    pub fn neg(self) ->  Self {
        Self(self.0.map(|x| x.neg()))
    }

    pub fn normalize(self) -> Self {
        let max = self.iter().map(|x| x.abs()).max_by(|a,b|a.total_cmp(&b)).unwrap_or(1.0);
        Self(self.0.map(|x| x / max))
    }

    pub fn nodc(&self) -> Self {
        let mut x = self.clone();
        let avg = x.iter().sum::<f64>() / 162.0;
        x.iter_mut().for_each(|x| *x -= avg);
        x
    }

    pub fn nomedian(&self) -> Self {
        let med =  self.median();
        let mut x = self.clone();
        x.iter_mut().for_each(|x| *x -= med);
        x
    }

    pub fn cap(&self, percentile: f64) -> Self {
        let mut sorted = self.0.map(|x|x.abs());
        sorted.sort_by(|a, b| a.total_cmp(b));
        let idx = (percentile * 162.0).max(0.0).min(161.0) as usize;
        let cap = sorted[idx];
        let mut x = self.clone();
        x.iter_mut().for_each(|x| *x = x.abs().min(cap) * x.signum());
        x
    }

    pub fn average(&self) -> f64 {
        self.iter().sum::<f64>() / 162.0
    }

    pub fn median(&self) -> f64 {
        let mut x = self.0;
        x.sort_by(|a, b| a.total_cmp(b));
        (x[80] + x[81]) / 2.0
    }

    pub fn deinterleave(&self) -> [(f32, f32); 81] {
        let mut ys = [(0f32, 0f32); 81];
        ys.iter_mut()
            .enumerate()
            .for_each(|(i, x)| *x = (self[i * 2] as f32, self[i * 2 + 1] as f32));
        ys
    }
}

impl Symbols<[Complex64; 4]> {
    pub fn snr_sync(&self) -> SNR {
        let mut snr = SNR::zero();
        for (idx, symbol) in self.0.iter().enumerate() {
            snr += SNR::with_sync(symbol, idx);
        }
        snr
    }

    pub fn snr_sync_coh<const N: usize>(&self) -> SNR {
        let mut snr = SNR::zero();
        let mut z = Symbols([[Complex64::zero();4]; SYMBOL_COUNT]);

        for i in 0..SYMBOL_COUNT {
            z[i] = if SYNC[i] == 0 {
                [self[i][0], self[i][2], self[i][1], self[i][3]]
            } else {
                [self[i][1], self[i][3], self[i][0], self[i][2]]
            };
        }

        z.array_windows::<N>().for_each(|win| {
            let cl = win.iter().fold((Complex64::zero(), Complex64::zero()), |mut acc, x| {
                acc.0 += x[0] + x[1];
                acc.1 += x[2] + x[3];
                acc
            });
            let a = cl.0.norm_sqr();
            let b = cl.1.norm_sqr();
            snr.signal += a - b;
            snr.noise += b + b;
        });

        snr.signal = snr.signal.max(0.0);
        snr
    }

    pub fn snr_sync_coh_arc<const N: usize>(&self) -> SNR {
        let mut snr = SNR::zero();
        let mut z = Symbols([[Complex64::zero();4]; SYMBOL_COUNT]);

        for i in 0..SYMBOL_COUNT {
            z[i] = if SYNC[i] == 0 {
                [self[i][0], self[i][2], self[i][1], self[i][3]]
            } else {
                [self[i][1], self[i][3], self[i][0], self[i][2]]
            };
        }

        for i in 0..81 {
            let fac = (i+1) as f64 / 81.0;
            z[i][0] *= fac;
            z[i][1] *= fac;
            z[i][2] *= fac;
            z[i][3] *= fac;
            z[161-i][0] *= fac;
            z[161-i][1] *= fac;
            z[161-i][2] *= fac;
            z[161-i][3] *= fac;
        }

        z.array_windows::<N>().for_each(|win| {
            let cl = win.iter().fold((Complex64::zero(), Complex64::zero()), |mut acc, x| {
                acc.0 += x[0] + x[1];
                acc.1 += x[2] + x[3];
                acc
            });
            let a = cl.0.norm_sqr();
            let b = cl.1.norm_sqr();
            snr.signal += a - b;
            snr.noise += b + b;
        });

        snr.signal = snr.signal.max(0.0);
        snr
    }

    pub fn snr_data(&self, n: u64) -> SNR {
        let x = Symbols::<u8>::encode(n);
        let mut snr = SNR::zero();
        for (symbol, data) in self.iter().zip(x.iter()) {
            snr += SNR::with_data(symbol, *data)
        }
        snr
    }

    pub fn sync(&self) -> Symbols<[f64;2]> {
        let mut d = Symbols([[0f64;2]; SYMBOL_COUNT]);
        for (x, y) in d.iter_mut().zip(self.iter()) {
            *x = [y[0].norm_sqr() + y[2].norm_sqr(), y[1].norm_sqr() + y[3].norm_sqr()];
        }
        d
    }

    pub fn data(&self) -> Symbols<[f64; 2]> {
        let mut d = Symbols([[0.0; 2]; SYMBOL_COUNT]);
        for ((x, y), i) in d.iter_mut().zip(self.iter()).zip(SYNC.iter()) {
            let sync = *i as usize;
            *x = [y[0 + sync].norm_sqr(), y[2 + sync].norm_sqr()];
        }
        d
    }

    pub fn energy(&self) -> Symbols<[f64;4]> {
        let mut d = Symbols([[0.0; 4]; SYMBOL_COUNT]);
        for (x, y) in d.iter_mut().zip(self.iter()) {
            x[0] += y[0].norm_sqr();
            x[1] += y[1].norm_sqr();
            x[2] += y[2].norm_sqr();
            x[3] += y[3].norm_sqr();
        }
        d
    }

    pub fn energy_min(&self) -> f64 {
        *self.energy().iter().map(|x| x.iter().min_by(|a,b|a.total_cmp(&b)).unwrap()).min_by(|a,b|a.total_cmp(b)).unwrap()
    }

    pub fn energy_max(&self) -> f64 {
        *self.energy().iter().map(|x| x.iter().max_by(|a,b|a.total_cmp(&b)).unwrap()).max_by(|a,b|a.total_cmp(b)).unwrap()
    }

    /// Computes outliers by comparing the energy of each symbol with the average
    /// of 4 neighbor symbols left and right of it. A symbol is considered an outlier
    /// if its energy is greater than twice the average of its neighbors.
    // pub fn outliers(&self) -> Symbols<bool> {
    //     const MARGIN: usize = 1;
    //     const THRESHOLD: f64 = 2.0;
    //     let mut result = Symbols([false; SYMBOL_COUNT]);
    //     let s = self.energy().summed();
    //     let global_avg = s.abs_avg();
    //     for (i, window) in s.windows(2*MARGIN + 1).enumerate() {
    //         let ls = window[0..MARGIN].iter().sum::<f64>();
    //         let rs = window[MARGIN+1..].iter().sum::<f64>();
    //         let local_avg = (ls + rs) / (2.0 * MARGIN as f64);
    //         if window[MARGIN] > global_avg && window[MARGIN] > THRESHOLD * local_avg {
    //             result[i + MARGIN] = true;
    //         }
    //     }
    //     result
    // }
    pub fn outliers(&self) -> Symbols<bool> {
        let mut result = Symbols([false; SYMBOL_COUNT]);
        // let mut es = self.clone().energy();
        // es.iter_mut().for_each(|x| x.sort_by(|a, b| a.total_cmp(b)));
        // let es = es.0.map(|x|x[3]);
        // let avg = es.iter().sum::<f64>() / 162.0;
        // let lower: Vec<f64> = es.iter().filter(|x| **x < avg).map(|x| x * x).collect();
        // let std = (lower.iter().sum::<f64>() / lower.len() as f64).sqrt();

        let mut es = self.data().diff();
        es.iter_mut().for_each(|x| *x = x.abs());
        let max = es.iter().map(|x|x.abs()).max_by(|a, b| a.total_cmp(b)).unwrap();
        es.iter_mut().for_each(|x| *x = 3. * *x / max);
        let avg = es.iter().sum::<f64>() / 162.0;
        let low_count = es.iter().filter(|x| **x < avg).count();
        let std = es.iter().filter(|x|**x < avg).map(|x|(x*x) / low_count as f64).sum::<f64>().sqrt();

        for i in 0..162 {
            result[i] = es[i] > avg + OUTLIER_XSIGMA * std;
        }

        result
    }
}

impl Symbols<[f64; 2]> {
    pub fn diff(&self) -> Symbols<f64> {
        Symbols(self.0.map(|x|x[0] - x[1]))
    }

    pub fn sqrt(self) -> Self {
        Self(self.0.map(|x| [x[0].signum() * x[0].abs().sqrt(), x[1].signum() * x[1].abs().sqrt()]))
    }

    pub fn normalize(self) -> Self {
        let max = self.iter().map(|x| x[0].abs().max(x[1].abs())).max_by(|a,b|a.total_cmp(&b)).unwrap_or(1.0);
        Self(self.0.map(|x| [x[0] / max, x[1] / max]))
    }

    pub fn without_outliers(mut self) -> Self {
        let outliers = self.diff().outliers();
        self.0.iter_mut().zip(outliers.0).for_each(|(a,b)| {
            if b {
                a[0] = 0.0;
                a[1] = 0.0;
            }
        });
        self
    }
}

impl Symbols<[f64; 4]> {
    pub fn sum(&self) -> f64 {
        self.sums().iter().sum()
    }

    pub fn summed(&self) -> Symbols<f64> {
        Symbols(self.map(|x| x.iter().sum()))
    }

    pub fn sums(&self) -> [f64;4] {
        let mut x = [0.0;4];
        for s in self.iter() {
            x[0] += s[0];
            x[1] += s[1];
            x[2] += s[2];
            x[3] += s[3];
        }
        x
    }

    pub fn level(mut self) -> Self {
        let deltas = self.sums().map(|x| x - self.sum() / 4.0);
        self.iter_mut().for_each(|x| x.iter_mut().zip(deltas.iter()).for_each(|(x,d)| * x -= d / SYMBOL_COUNT as f64));
        self
    }

    // pub fn snr_sync(&self) -> SNR {
    //     let mut snr = SNR::zero();
    //     for (idx, symbol) in self.0.iter().enumerate() {
    //         snr += SNR::with_syncf(symbol, idx);
    //     }
    //     snr
    // }

    // pub fn snr_data(&self, n: u64) -> SNR {
    //     let x = Symbols::<u8>::encode(n);
    //     let mut snr = SNR::zero();
    //     for (symbol, data) in self.iter().zip(x.iter()) {
    //         snr += SNR::with_data(symbol, *data)
    //     }
    //     snr
    // }
}

impl<T> Deref for Symbols<T> {
    type Target = [T; SYMBOL_COUNT];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Symbols<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl <T: Copy + Default> Default for Symbols<T> {
    fn default() -> Self {
        Self([Default::default(); SYMBOL_COUNT])
    }
}
