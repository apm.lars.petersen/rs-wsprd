use crate::constants::{POLY1, POLY1_REV, POLY2, POLY2_REV};
use crate::symbols::Symbols;
use crate::{flip, ite};
use std::collections::{BinaryHeap, HashMap};

// const ITER_POW: usize = 23;
// const ITER_MAX: usize = 1 << ITER_POW;

const QUEUE_POW: usize = 24;
const QUEUE_MAX: usize = 1 << QUEUE_POW;

pub struct ChiStar;

impl ChiStar {
    pub fn search(symbols: &Symbols<f64>, depth: usize) -> ChiStarResult {
        const IDXS: usize = 8;
        const IDXM: u64 = (1 << IDXS) - 1;
        const EVIL: u64 = 8;

        let iter_max: usize = 1 << depth;

        let xs81 = symbols.deinterleave();
        let ns81 = symbols.nomedian().cap(0.95).deinterleave();

        let mut rs = ChiStarResult::default();
        let mut hf = BinaryHeap::<Node>::with_capacity(QUEUE_MAX);
        let mut hb = BinaryHeap::<Node>::with_capacity(QUEUE_MAX);
        let mut sf = HashMap::<u32, (f32, u64)>::new();
        let mut sb = HashMap::<u32, (f32, u64)>::new();
        let mut bf = f32::MIN;
        let mut bb = f32::MIN;
        let mut bt = f32::MIN;
        let mut tf = false;
        let mut tb = false;

        hf.push(Node::default());
        hb.push(Node::default());

        let now = std::time::Instant::now();

        while rs.iterations < iter_max && (!tf || !tb) {
            let fw = tb || rs.iterations % 2 == 0;
            let p1 = ite!(fw, POLY1, POLY1_REV);
            let p2 = ite!(fw, POLY2, POLY2_REV);
            let h0 = ite!(fw, &mut hf, &mut hb);
            let t0 = ite!(fw, &mut tf, &mut tb);
            let (s0, s1) = flip!(fw, &mut sf, &mut sb);
            let (b0, b1) = flip!(fw, &mut bf, &mut bb);
            rs.iterations += 1;

            if h0.len() == QUEUE_MAX {
                let mut q = std::mem::take(h0).into_vec();
                q.sort_by(|a, b| b.mtr.total_cmp(&a.mtr));
                q.truncate(QUEUE_MAX / 2);
                *h0 = q.into();
                rs.shrinks += 1;
            }

            if let Some(n) = h0.pop() {
                let idx = (n.dat & IDXM) as usize;
                if idx == 41 {
                    let dat = n.dat >> IDXS;
                    let srs = dat as u32;
                    *b0 = b0.max(n.mtr);
                    if let Some((m, d)) = s1.get(&srs) {
                        let (d0, d1) = flip!(fw, dat, *d);
                        let mtr = n.mtr + m;
                        if mtr > bt {
                            bt = mtr;
                            rs.data = (d0.reverse_bits() >> 23) | (d1 << 9);
                        }
                    } else {
                        s0.insert(srs.reverse_bits(), (n.mtr, dat));
                        *t0 = n.mtr + *b1 < bt;
                    }
                } else if n.dat != EVIL {
                    let (a, b) = ns81[ite!(fw, idx, 80 - idx)];
                    let srsn = n.dat >> IDXS;
                    let srs0 = srsn << 1;
                    let srs1 = srs0 | 1;
                    let mut n0: Node = n;
                    let mut n1: Node = n;
                    n0.mtr += Self::err(srs0 as u32, p1, p2, a, b);
                    n1.mtr += Self::err(srs1 as u32, p1, p2, a, b);
                    n0.dat = (srs0 << IDXS) | (idx + 1) as u64;
                    n1.dat = (srs1 << IDXS) | (idx + 1) as u64;
                    h0.push(n0);
                    h0.push(n1);
                }
            }
        }

        rs.duration = now.elapsed();
        rs.complete = tf && tb;
        rs.likelyhood = Self::likelyhood(&xs81, rs.data);
        rs
    }

    pub fn likelyhood(xs: &[(f32, f32); 81], data: u64) -> f64 {
        let mut dat = data;
        let mut srs = 0u32;
        let mut max = 0.0;
        let mut err = 0.0;
        for (a, b) in xs {
            srs <<= 1;
            srs |= dat as u32 & 1;
            max += a.abs() + b.abs();
            err += Self::err(srs, POLY1, POLY2, *a, *b);
            dat >>= 1;
        }
        ((max + err) / max) as f64
    }

    #[inline]
    fn err(srs: u32, poly1: u32, poly2: u32, a: f32, b: f32) -> f32 {
        let f = |x: u32| ite!(x.count_ones() & 1 == 0, 1.0, -1.0);
        (a * f(srs & poly1)).min(0.0) + (b * f(srs & poly2)).min(0.0)
    }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct ChiStarResult {
    pub data: u64,
    pub shrinks: usize,
    pub iterations: usize,
    pub complete: bool,
    pub duration: std::time::Duration,
    pub likelyhood: f64,
}

#[derive(Debug, Default, Clone, Copy)]
struct Node {
    mtr: f32,
    dat: u64,
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.mtr == other.mtr
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.mtr.partial_cmp(&other.mtr)
    }
}

impl Eq for Node {}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.mtr.total_cmp(&other.mtr)
    }
}
