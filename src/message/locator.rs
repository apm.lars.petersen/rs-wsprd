use std::str::FromStr;

use super::util::*;

#[derive(Clone, Copy)]
pub struct Locator(pub u64);

impl FromStr for Locator {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        locator_to_num(s).map(Self).ok_or("invalid locator")
    }
}

impl std::fmt::Display for Locator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let n = self.0;

        let n1 = n / 180;
        let n2 = n % 180;

        let c4 = ((n2 % 10) as u8 + b'0') as char;
        let c2 = ((n2 / 10) as u8 + b'A') as char;
        let c3 = (9 - (n1 % 10) as u8 + b'0') as char;
        let c1 = (17 - (n1 / 10) as u8 + b'A') as char;

        write!(f, "{c1:}{c2:}{c3:}{c4:}")
    }
}

impl std::fmt::Debug for Locator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:015b}", self.0)
    }
}

fn locator_to_num(s: &str) -> Option<u64> {
    let mut cs = s.chars();
    let c1 = char_to_num(cs.next()?)?;
    let c2 = char_to_num(cs.next()?)?;
    let c3 = char_to_num(cs.next()?)?;
    let c4 = char_to_num(cs.next()?)?;
    check(c1 >= 10 && c1 <= 27)?;
    check(c2 >= 10 && c2 <= 27)?;
    check(c3 <= 9)?;
    check(c4 <= 9)?;
    let c1 = c1 - 10;
    let c2 = c2 - 10;
    let n = 179 - 10 * c1 - c3;
    let n = n * 180 + 10 * c2 + c4;
    Some(n)
}
