pub fn char_to_num(c: char) -> Option<u64> {
    if c >= '0' && c <= '9' {
        Some(c as u64 - '0' as u64)
    } else if c >= 'A' && c <= 'Z' {
        Some(c as u64 + 10 - 'A' as u64)
    } else if c == ' ' {
        Some(36)
    } else {
        None
    }
}

pub fn num_to_char(n: u64) -> Option<char> {
    if n <= 9 {
        Some((n as u8 + b'0') as char)
    } else if n <= 35 {
        Some((n as u8 - 10 + b'A') as char)
    } else if n == 36 {
        Some(' ')
    } else {
        None
    }
}

#[must_use]
pub fn check(condition: bool) -> Option<()> {
    if condition {
        Some(())
    } else {
        None
    }
}