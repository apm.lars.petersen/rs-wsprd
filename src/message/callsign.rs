use super::util::*;
use std::str::FromStr;

#[derive(Clone, Copy)]
pub struct Callsign(pub u64);

impl FromStr for Callsign {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        callsign_to_num(s).map(Self).ok_or("invalid callsign")
    }
}

impl std::fmt::Display for Callsign {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let n = self.0;
        let x1 = n % 27 + 10;
        let n = n / 27;
        let x2 = n % 27 + 10;
        let n = n / 27;
        let x3 = n % 27 + 10;
        let n = n / 27;
        let x4 = n % 10;
        let n = n / 10;
        let x5 = n % 36;
        let n = n / 36;
        let x6 = n % 37;
        for x in [x6, x5, x4, x3, x2, x1] {
            let c = num_to_char(x).unwrap();
            if c != ' ' {
                write!(f, "{c:}")?;
            }
        }
        Ok(())
    }
}

impl std::fmt::Debug for Callsign {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:028b}", self.0)
    }
}

fn callsign_to_num(s: &str) -> Option<u64> {
    const SP: char = ' ';
    let mut cs = s.chars();
    let mut c1 = char_to_num(cs.next().unwrap_or(SP))?;
    let mut c2 = char_to_num(cs.next().unwrap_or(SP))?;
    let mut c3 = char_to_num(cs.next().unwrap_or(SP))?;
    let mut c4 = char_to_num(cs.next().unwrap_or(SP))?;
    let mut c5 = char_to_num(cs.next().unwrap_or(SP))?;
    let mut c6 = char_to_num(cs.next().unwrap_or(SP))?;
    if c2 <= 10 {
        c6 = c5;
        c5 = c4;
        c4 = c3;
        c3 = c2;
        c2 = c1;
        c1 = 36;
    }
    check(c2 <= 35)?;
    check(c3 <= 9)?;
    check(c4 >= 10)?;
    check(c5 >= 10)?;
    check(c6 >= 10)?;
    let n1 = c1;
    let n2 = n1 * 36 + c2;
    let n3 = n2 * 10 + c3;
    let n4 = n3 * 27 + (c4 - 10);
    let n5 = n4 * 27 + (c5 - 10);
    let n6 = n5 * 27 + (c6 - 10);
    Some(n6)
}
