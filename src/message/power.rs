
use std::str::FromStr;

#[derive(Clone, Copy)]
pub struct Power(pub u64);

impl FromStr for Power {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse().ok().filter(|n| *n <= 60).map(Self).ok_or("invalid power")
    }
}

impl std::fmt::Debug for Power {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "_{:06b}", self.0)
    }
}


impl std::fmt::Display for Power {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
