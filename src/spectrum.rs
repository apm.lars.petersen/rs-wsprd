use num_complex::Complex64;
use rayon::prelude::*;
use rustfft::algorithm::Radix4;
use rustfft::num_traits::Zero;
use rustfft::Fft;
use rustfft::FftDirection;

use crate::constants::*;
use crate::location::Location;
use crate::lrr::LRR;
use crate::rate::Rate;
use crate::signal::*;
use crate::snr::SNR;
use crate::symbols::Symbols;

#[derive(Debug, Clone)]
pub struct Spectrum {
    pub config: SpectrumConfig,
    pub signal: Signal<Complex64>,
}

impl Spectrum {
    pub fn new(config: SpectrumConfig, signal: &Signal<f64>) -> Self {
        let isr = config.isr.min(signal.rate());

        let freq_pad = SYMBOL_DISTANCE * 2.0;
        let freq_hsr = isr.hz() as f64 / 2. + freq_pad;
        let freq_min = config.frequency - freq_hsr;
        let freq_max = config.frequency + freq_hsr;

        let blk_len = isr.block_len();
        let blk_len_raw = signal.rate().block_len();

        let signal = signal.complex();
        let signal = signal.resize((BLOCK_COUNT + 1) * blk_len_raw);
        let signal = signal.bandpass(freq_min, freq_max);
        let signal = signal.shift(config.frequency);
        let signal = signal.decimate(blk_len_raw / blk_len);

        Self {
            config,
            signal: signal.clone(),
        }
    }

    pub fn candidates(&self) -> Candidates {
        let isr = self.config.isr.min(self.signal.rate());

        let delay_count = 1 << self.config.tempres;
        let shift_count = 1 << self.config.spectres;

        let blk_len = isr.block_len();

        let bin_count = (self.config.bandwidth / SYMBOL_DISTANCE) as usize;
        let bin_start = blk_len - bin_count / 2;

        let fft = Radix4::<f64>::new(blk_len, FftDirection::Forward);

        let matches_id = || vec![];
        let mut matches = (0..shift_count)
            .into_par_iter()
            .map(|shift_i| {
                let shift = -(shift_i as f64 / shift_count as f64) * SYMBOL_DISTANCE;

                let mut mts = vec![Match::default(); bin_count];
                let mut blk = vec![[Complex64::zero();256];BLOCK_COUNT];

                let signal = self.signal.clone().shift_and_drift(shift, 0.0);

                for delay_i in 0..delay_count {
                    let delay = delay_i * blk_len / delay_count;
                    let signal = &signal[delay..];

                    let mut snlr = vec![[(SNR::zero(), SNR::zero(), LRR::zero()); BLOCK_EXTRA]; bin_count];

                    for blk_i in 0..BLOCK_COUNT {
                        blk[blk_i].copy_from_slice(&signal[blk_i * blk_len..][..blk_len]);
                        fft.process(&mut blk[blk_i]);
                    }

                    for blk_i in 0..BLOCK_COUNT {
                        for bin_i in 0..bin_count {
                            let s0 = blk[blk_i][(bin_start + bin_i + 0) % blk_len];
                            let s1 = blk[blk_i][(bin_start + bin_i + 1) % blk_len];
                            let s2 = blk[blk_i][(bin_start + bin_i + 2) % blk_len];
                            let s3 = blk[blk_i][(bin_start + bin_i + 3) % blk_len];
                            let ss = [s0, s1, s2, s3];
                            for i_extra in 0..BLOCK_EXTRA {
                                if blk_i >= i_extra && (blk_i - i_extra) < SYMBOL_COUNT {
                                    let (snr, _,lrr) = &mut snlr[bin_i][i_extra];
                                    *snr += SNR::with_sync(&ss, blk_i - i_extra) * ARC64[blk_i - i_extra];
                                    *lrr += LRR::new_complex(s0, s1, s2, s3);
                                }
                            }
                        }
                    }

                    let mut zzz = Symbols::<[Complex64; 2]>::new();

                    for bin_i in 0..bin_count {
                        for i_extra in 0..BLOCK_EXTRA {
                            for blk_i in 0..SYMBOL_COUNT {
                                let x0 = blk[blk_i + i_extra][(bin_start + bin_i + 0) % blk_len] * ARC64[blk_i];
                                let x1 = blk[blk_i + i_extra][(bin_start + bin_i + 1) % blk_len] * ARC64[blk_i];
                                let x2 = blk[blk_i + i_extra][(bin_start + bin_i + 2) % blk_len] * ARC64[blk_i];
                                let x3 = blk[blk_i + i_extra][(bin_start + bin_i + 3) % blk_len] * ARC64[blk_i];
                                if SYNC[blk_i] == 0 {
                                    zzz[blk_i][0] = x0 + x2;
                                    zzz[blk_i][1] = x1 + x3;
                                } else {
                                    zzz[blk_i][0] = x1 + x3;
                                    zzz[blk_i][1] = x0 + x2;
                                }
                            }

                            let (_, snr, _) = &mut snlr[bin_i][i_extra];
                            zzz.array_windows::<3>().for_each(|win| {
                                let cl = win.iter().fold((Complex64::zero(), Complex64::zero()), |mut acc, x| {
                                    acc.0 += x[0];
                                    acc.1 += x[1];
                                    acc
                                });
                                let a = cl.0.norm_sqr();
                                let b = cl.1.norm_sqr();
                                snr.signal += a - b;
                                snr.noise += b + b;
                            });
                            snr.signal = snr.signal.max(0.0);
                        }
                    }

                    for (bin_i, sn) in snlr.into_iter().enumerate() {
                        let mut max_blk: usize = 0;
                        let mut max_snr: SNR = SNR::default();
                        let mut max_snr2: SNR = SNR::default();
                        let mut max_lrr: LRR = LRR::new(1.0);
                        for (blk_i, (mut snr,mut snr2, lrr)) in sn.into_iter().enumerate() {
                            snr.signal = snr.signal.max(0.0);
                            snr2.signal = snr2.signal.max(0.0);
                            if snr >= max_snr {
                                max_blk = blk_i;
                                max_snr = snr;
                                max_snr2 = snr2;
                                max_lrr = lrr;
                            }
                        }
                        let x = &mut mts[bin_i];
                        if max_snr >= x.snr {
                            let bin = bin_i as isize - bin_count as isize / 2;
                            let blk = max_blk;
                            x.snr = max_snr;
                            x.snr2 = max_snr2;
                            x.lrr = max_lrr;
                            x.loc.time = blk as f64 * SYMBOL_DURATION + delay as f64 / isr.hz() as f64;
                            x.loc.freq = bin as f64 * SYMBOL_DISTANCE + self.config.frequency - shift  + SYMBOL_DISTANCE * 1.5;
                            x.loc.drift = 0.0;
                        }
                    }
                }
                mts
            })
            .fold(matches_id, Spectrum::mts_insert2)
            .reduce(matches_id, Spectrum::mts_insert2);

        matches.sort_by(|a, b| a.loc.freq.total_cmp(&b.loc.freq));

        Candidates {
            spectrum: matches,
        }
    }

    pub fn extract(&self, loc: &Match) -> Symbols<[Complex64; 4]> {
        let blk_len = self.config.isr.block_len();
        let fft = Radix4::<f64>::new(blk_len, FftDirection::Forward);
        let off = (loc.loc.time * self.signal.rate().hz() as f64) as usize;
        
        let freq = loc.loc.freq - 1500.0 - 1.5 * SYMBOL_DISTANCE;
        let shift = freq.rem_euclid(SYMBOL_DISTANCE);
        let bin = freq.div_euclid(SYMBOL_DISTANCE) as isize;
        let bin = ((2 * blk_len) as isize + bin) as usize;

        let mut sym: Symbols<[Complex64; 4]> = Symbols::new();
        let mut sig = self.signal.clone().shift_and_drift(-shift, loc.loc.drift);

        for i in 0..SYMBOL_COUNT {
            let off = off + i * blk_len;
            let blk = &mut sig[off..][..blk_len];
            fft.process(blk);
            sym[i][0] = blk[(bin + 0) % blk_len];
            sym[i][1] = blk[(bin + 1) % blk_len];
            sym[i][2] = blk[(bin + 2) % blk_len];
            sym[i][3] = blk[(bin + 3) % blk_len];
        }

        sym
    }

    pub fn energy(&self) -> f64 {
        let blk_len = self.config.isr.block_len();
        let fft = Radix4::<f64>::new(blk_len, FftDirection::Forward);
        let of1 = self.config.isr.hz(); // 1 second offset;

        let mut sig = self.signal.clone();
        let mut sum = 0.0;

        for i in 0..SYMBOL_COUNT {
            let off = of1 + i * blk_len;
            let blk = &mut sig[off..][..blk_len];
            fft.process(blk);
            for bin_i in 0..135 {
                sum += blk[bin_i].norm_sqr();
                sum += blk[blk_len - bin_i - 1].norm_sqr();
            }
        }

        sum
    }

    fn mts_insert2(mut locs: Vec<Match>, shift: Vec<Match>) -> Vec<Match> {
        locs.extend(shift);
        locs
    }
}

#[derive(Debug, Clone)]
pub struct SpectrumConfig {
    pub isr: Rate,
    pub frequency: f64,
    pub bandwidth: f64,

    pub tempres: usize,
    pub spectres: usize,
    pub driftres: usize,

    pub snr_threshold: SNR,
    pub lrr_threshold: LRR,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Match {
    pub snr: SNR,
    pub snr2: SNR,
    pub lrr: LRR,
    pub loc: Location
}

impl std::fmt::Display for Match {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let freq = self.loc.freq;
        let time = self.loc.time - 1.0;
        write!(
            f,
            "{:7.3}Hz  {:+2.4}Hz/s  {:+4.3}s  {:+5.1}dB {:+5.1}dB  {:5.1}! ",
            freq,
            self.loc.drift,
            time,
            self.snr.db(),
            self.snr2.db(),
            self.lrr
        )
    }
}

pub struct Candidates {
    pub spectrum: Vec<Match>,
}

impl Candidates {
    pub fn maxima(&self) -> Vec<Match> {
        let mut maxima = vec![];
        for w in self.spectrum.windows(3) {
            let r0 = w[0].snr;
            let r1 = w[1].snr;
            let r2 = w[2].snr;
            if r0 < r1 && r1 > r2 && r1 >= SNR::new(0.1) {
                maxima.push(w[1]);
            }
        }
        maxima
    }
}
