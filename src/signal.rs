use num_complex::{Complex, Complex64};
use rustfft::*;
use std::f64::consts::PI;
use std::ops::{Deref, DerefMut, Range};

use crate::rate::Rate;

#[derive(Debug, Clone)]
pub struct Signal<T = Complex64> {
    rate: Rate,
    samples: Vec<T>,
}

impl<T: Clone + Default> Signal<T> {
    pub fn new(rate: Rate, len: usize) -> Self {
        Self {
            rate,
            samples: vec![T::default(); len],
        }
    }

    pub fn slice(&self, range: Range<usize>) -> Self {
        Self {
            rate: self.rate,
            samples: self.samples[range].to_vec(),
        }
    }

    pub fn rate(&self) -> Rate {
        self.rate
    }

    pub fn resize(mut self, len: usize) -> Self {
        self.samples.resize(len, Default::default());
        self
    }

    pub fn decimate(&self, factor: usize) -> Self {
        Self {
            rate: Rate::new(self.rate.hz() / factor).unwrap(),
            samples: self.iter().step_by(factor).cloned().collect(),
        }
    }
}

impl Signal<f64> {
    pub fn normalize(&mut self, amplitude: f64) {
        if let Some(max) = self.samples.iter().max_by(|x, y| f64::total_cmp(&x, &y)) {
            let factor = amplitude / max;
            self.samples.iter_mut().for_each(|x| *x = *x * factor);
        }
    }

    pub fn complex(&self) -> Signal<Complex64> {
        Signal {
            rate: self.rate,
            samples: self.samples.iter().map(|x| Complex::new(*x, 0.0)).collect(),
        }
    }

    pub fn load(filename: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let mut reader = hound::WavReader::open(filename)?;
        let spec = reader.spec();
        let rate = spec.sample_rate as usize;
        let mut samples: Vec<f64> = vec![];
        match spec.sample_format {
            hound::SampleFormat::Float => {
                let mut reader = reader.samples::<f32>();
                while let Some(a) = reader.next() {
                    samples.push(a? as f64);
                }
            }
            hound::SampleFormat::Int => {
                let q = 2u32.pow(spec.bits_per_sample as u32 - 1) as f64;
                let mut reader = reader.samples::<i32>();
                while let Some(a) = reader.next() {
                    let x = a? as f64 / q;
                    samples.push(x);
                }
            }
        }

        Ok(Self { rate: Rate::new(rate)?, samples })
    }

    pub fn save(&self, filename: &str) -> Result<(), Box<dyn std::error::Error>> {
        let spec = hound::WavSpec {
            channels: 1,
            sample_rate: self.rate as u32,
            bits_per_sample: 32,
            sample_format: hound::SampleFormat::Float,
        };
        let mut writer = hound::WavWriter::create(filename, spec)?;
        for sample in &self.samples {
            writer.write_sample(*sample as f32)?
        }
        writer.finalize()?;
        Ok(())
    }
}

impl Signal<Complex64> {
    pub fn real(&self) -> Signal<f64> {
        Signal {
            rate: self.rate,
            samples: self.samples.iter().map(|x| x.re).collect(),
        }
    }

    pub fn conj(&mut self) {
        self.iter_mut().for_each(|x| *x = x.conj())
    }

    pub fn normalize(&mut self, amplitude: f64) {
        let max = self.iter().copied().map(Complex64::norm).max_by(f64::total_cmp).unwrap_or(1.0);
        let factor = amplitude / max;
        self.samples.iter_mut().for_each(|x| *x = *x * factor);
    }

    pub fn add(&mut self, other: &Self) {
        assert!(self.rate == other.rate);
        self.iter_mut()
            .zip(other.iter())
            .for_each(|(x, y)| *x += *y);
    }

    pub fn bandpass(mut self, min: f64, max: f64) -> Self {
        let mut planner = FftPlanner::<f64>::new();
        let fft = planner.plan_fft_forward(self.len());
        let ifft = planner.plan_fft_inverse(self.len());
        let buf = &mut self.samples;
        let len = buf.len();
        fft.process(buf);
        // Remove mirror frequencies
        for i in len / 2..len - 1 {
            buf[i] = Complex::new(0.0, 0.0);
        }
        // Remove lower frequencies
        let n = len;
        for i in 0..(min as usize * n / self.rate.hz()) {
            buf[i] = Complex::new(0.0, 0.0);
        }
        // Remove higher frequencies
        for i in (max as usize * n / self.rate.hz())..n / 2 {
            buf[i] = Complex::new(0.0, 0.0);
        }
        ifft.process(buf);
        buf.iter_mut().for_each(|x| *x = x.unscale(len as f64));
        self
    }

    /// Shift by Hz
    pub fn shift(mut self, df: f64) -> Self {
        let n = self.samples.len();

        let df = Complex::from_polar(1.0, 2.0 * PI * df / self.rate.hz() as f64);
        let mut phi = Complex::new(1.0, 0.0);

        for i in 0..n {
            self.samples[i] *= phi;
            phi *= df;
            phi = phi.unscale(phi.norm());
        }

        self
    }

    /// Shift by Hz and drift by Hz/s
    pub fn shift_and_drift(mut self, shift: f64, drift: f64) -> Self {
        let rt = self.rate.hz() as f64;
        let dt = Complex::from_polar(1.0, 2.0 * PI * shift / rt);
        let dd = Complex::from_polar(1.0, 2.0 * PI * drift / (rt * rt));

        let mut phi = Complex::new(1.0, 0.0);
        let mut psi = Complex::new(1.0, 0.0);
        let mut chi = Complex::new(1.0, 0.0);

        for sample in self.samples.iter_mut() {
            *sample *= phi * chi;
            phi *= dt;
            psi *= dd;
            chi *= psi;
            // Avoid numerical error accumulation
            phi = phi.unscale(phi.norm());
            psi = psi.unscale(psi.norm());
            chi = chi.unscale(chi.norm());
        }

        self
    }

    /// Shift by Hz and drift by Hz/s
    pub fn shift_and_drift_mut(&mut self, shift: f64, drift: f64) {
        let rt = self.rate.hz() as f64;
        let dt = Complex::from_polar(1.0, 2.0 * PI * shift / rt);
        let dd = Complex::from_polar(1.0, 2.0 * PI * drift / (rt * rt));

        let mut phi = Complex::new(1.0, 0.0);
        let mut psi = Complex::new(1.0, 0.0);
        let mut chi = Complex::new(1.0, 0.0);

        for sample in self.samples.iter_mut() {
            *sample *= phi * chi;
            phi *= dt;
            psi *= dd;
            chi *= psi;
            // Avoid numerical error accumulation
            // phi = phi.unscale(phi.norm());
            // psi = psi.unscale(psi.norm());
            // chi = chi.unscale(chi.norm());
        }
    }
}

impl<T> Deref for Signal<T> {
    type Target = Vec<T>;

    fn deref(&self) -> &Self::Target {
        &self.samples
    }
}

impl<T> DerefMut for Signal<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.samples
    }
}
