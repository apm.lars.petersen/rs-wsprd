use cpal::{traits::{DeviceTrait, StreamTrait}, Stream, StreamError};
use std::sync::{Arc, Mutex};

pub trait QdxConsumer: Sync + Send + 'static {
    fn on_data(&mut self, data: &[f32]) -> Result<(), Box<dyn std::error::Error>>;
    fn on_error(&mut self, err: String);
}

pub struct Qdx {
    consumers: Arc<Mutex<Vec<Box<dyn QdxConsumer>>>>,
    stream: Stream
}

impl Qdx {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {

        use cpal::traits::HostTrait;
        let host = cpal::default_host();
        let mut devices = host.input_devices()?;
        let dev = devices.find(|d| d.name().unwrap_or("".to_string()).contains("QDX"));
        let dev = dev.ok_or("QDX device not found")?;
    
        let config = dev.default_input_config()?;
        let config = config.config();
        let consumers: Arc<Mutex<Vec<Box<dyn QdxConsumer>>>> = Arc::new(Mutex::new(Vec::new()));

        let cs1 = consumers.clone();
        let on_data = move |xs: &[f32], _info: &cpal::InputCallbackInfo| {
            for c in cs1.lock().unwrap().iter_mut() {
                let _: &mut Box<dyn QdxConsumer> = c;
                let _ = c.on_data(xs);
            }
        };

        let cs2 = consumers.clone();
        let on_error = move |err: StreamError| {
            let err = format!("{:?}", err);
            for c in cs2.lock().unwrap().iter_mut() {
                let _: &Box<dyn QdxConsumer> = c;
                c.on_error(err.clone());
            }
        };

        let stream = dev.build_input_stream(&config, on_data, on_error, None)?;
        stream.play()?;

        Ok(Self { consumers, stream })
    }

    pub fn consume(&mut self, consumer: Box<dyn QdxConsumer>) {
        self.consumers.lock().unwrap().push(consumer)
    }
}
