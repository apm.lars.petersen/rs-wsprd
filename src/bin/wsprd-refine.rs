#![feature(array_windows)]
use core::f64;
use phenol::{
    chistar::ChiStar, location::Location, lrr::LRR, plot::plot_signal, refine::refine, signal::Signal, snr::SNR, spectrum::Match, symbols::Symbols
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let signal = Signal::<f64>::load("samples/wspr_14260087.wav")?;
    let signal = signal.complex();
    let signal = signal.resize(8192 * (162 + 5));
    let signal = signal.bandpass(1400.0, 1600.0);
    let signal = signal.shift(-1500.0);
    let signal = signal.decimate(32);

    let l2 = Location {
        freq: 1494.9588775634766, time: 1.088, drift: 0.
    };
    //const DATA: u64 = 0b10000111010101111111101011110011011111011001101111;
    const DATA: u64 = 0b11101010101110110111100011010111100000011101010101;
    let res = refine(&signal, &l2, Some(DATA));

    let data = res.symbols.data().unshuffle().diff();
    let dec = ChiStar::search(&data, 23);
    println!(
        "Max: S {:9.8} D {:9.8} CHR {:9.6} | {:?}",
        res.location.freq, res.location.drift, res.snr, res.ij
    );
    let m = Match {
        snr: SNR::zero(),
        snr2: SNR::zero(),
        lrr: LRR::zero(),
        loc: Location {
            freq: l2.freq,
            time: l2.time,
            drift: 0.0,
        }
    };
    plot_signal(0, m, &res.symbols, &dec).unwrap();

    let mut z = Symbols::<f64>::new();
    for i in 0..81 {
        let fac = (i+1) as f64 / 81.0;
        z[i] = fac;
        z[161-i] = fac;
    }

    println!("{:?}", z);

    Ok(())
}

