use clap::Parser;
use num_complex::Complex;
use phenol::rate::Rate;
use phenol::signal::*;
use phenol::symbols::Symbols;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(value_name = "FILE.wav")]
    file: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const RATE: Rate = Rate::Hz12k;
    const SECONDS: usize = 120;

    let mut plan = rustfft::FftPlanner::<f64>::new();
    let fft_8192 = plan.plan_fft_inverse(8192);

    let n: u64 = 0b11101010001101111111100100010011011101101001101111;
    let fs: f64 = 0.125;
    let sym = Symbols::<u8>::encode(n);
    let tone = Signal::new(RATE, 8192);
    let mut tones = [tone.clone(), tone.clone(), tone.clone(), tone.clone()];
    for i in 0..4 {
        tones[i][1024 + i] = Complex::new(1.0, 0.0);
        fft_8192.process(&mut tones[i]);
        let x = tones[i].real();
        x.save(&format!("tone{}.wav", i))?;
    }

    let mut n = Signal::new(RATE, RATE.hz() * SECONDS);
    let min = 500 * SECONDS;
    let max = 3000 * SECONDS;
    for i in min..max {
        n[i] = Complex::from_polar(0.05 / 120.0 * (8192.0 / 12000.0), rand::random::<f64>() * 2.0 * std::f64::consts::PI);
    }
    let fft = plan.plan_fft_inverse(n.len());
    fft.process(&mut n);
    let m = n.real();
    m.save("call-noise.wav")?;

    let mut s = n.clone();
    for i in 0..162 {
        let r = rand::random::<f64>();
        // let phi = Complex::from_polar(1.0, r * 2.0 * std::f64::consts::PI);
        println!("{:?}", r);
        for j in 0..8192 {
            s[i*8192 + j] = tones[sym[i] as usize][j] ; // * phi;
        }
    }
    s.normalize(fs);
    s.add(&n);
    let s = s.real();
    s.save("call-phase.wav")?;

    let mut s = n.clone();
    for i in 0..162 {
        let r = rand::random::<f64>();
        let phi = Complex::from_polar(1.0, r * 2.0 * std::f64::consts::PI);
        println!("{:?}", r);
        for j in 0..8192 {
            s[i*8192 + j] = tones[sym[i] as usize][j] * phi;
        }
    }
    s.normalize(fs);
    s.add(&n);
    let s = s.real();
    s.save("call-nophase.wav")?;


    Ok(())
}
