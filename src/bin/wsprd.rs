#![feature(iter_map_windows)]
#![feature(iter_array_chunks)]
use std::collections::HashSet;
use std::sync::Arc;
use std::sync::Mutex;
use clap::ArgAction;
use clap::Parser;
use phenol::constants::*;
use phenol::list::List;
use phenol::lrr::LRR;
use phenol::message::Message;
use phenol::chistar::ChiStar;
use phenol::plot::plot_signal;
use phenol::rate::Rate;
use phenol::signal::*;
use phenol::snr::SNR;
use phenol::spectrum::*;
use rayon::iter::IntoParallelRefIterator;
use rayon::iter::ParallelIterator;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Input file (mono, 12kHz/48kHz, 16bit PCM or 24bit IEEE float)
    #[arg(value_name = "FILE.wav")]
    file: String,
    /// Center frequency (Hz)
    #[arg(short='f', long = "frequency", value_name = "Hz", default_value_t = DEFAULT_CENTERFREQ)]
    frequency: f64,
    /// Bandwidth (Hz)
    #[arg(short='b', long = "bandwidth", value_name = "Hz", default_value_t = DEFAULT_BANDWIDTH)]
    bandwidth: f64,
    /// Temporal resolution (power of 2)
    #[arg(short='t', long = "tempres", value_name = "nat", default_value_t = DEFAULT_TIMERES)]
    tempres: usize,
    /// Spectral resolution (power of 2)
    #[arg(short='s', long = "spectres", value_name = "nat", default_value_t = DEFAULT_SPECTRES)]
    spectres: usize,
    /// Drift resolution (power of 2)
    #[arg(short='d', long = "driftres", value_name = "nat",  default_value_t = DEFAULT_DRIFTRES)]
    driftres: usize,
    /// SNR threshold
    #[arg(long = "snr", value_name = "ratio",  default_value_t = DEFAULT_SNR_THRESHOLD)]
    snr_threshold: SNR,
    /// LRR threshold
    #[arg(long = "lrr", value_name = "ratio",  default_value_t = DEFAULT_LRR_THRESHOLD)]
    lrr_threshold: LRR,
    /// Intermediate sample rate
    #[arg(long, value_name = "rate", default_value_t=Rate::Hz0k375)]
    isr: Rate,
    /// Verbose mode
    #[arg(short = 'v', long = "verbose", action=ArgAction::SetTrue)]
    verbose: bool,
    #[arg(long = "fmin")]
    fmin: Option<f64>,
    #[arg(long = "fmax")]
    fmax: Option<f64>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    let signal = Signal::<f64>::load(&args.file)?;

    let config = SpectrumConfig {
        isr: args.isr,
        frequency: args.frequency,
        bandwidth: args.bandwidth,
        tempres: args.tempres,
        spectres: args.spectres,
        driftres: args.driftres,
        snr_threshold: args.snr_threshold,
        lrr_threshold: args.lrr_threshold,
    };

    let list = List::load()?;
    let missing: Arc<Mutex<HashSet<String>>> = Arc::new(Mutex::new(vec![
        "DL0PBS".to_string(),
        "PA0MIK".to_string(),
        "SO2O".to_string(),
        "HB9AK".to_string(),
        "G8PY".to_string(),
        "DG8GAD".to_string(),
        "DG7NFX".to_string(),
        "DL2HAD".to_string(),
        "PD0KT".to_string(),
        "IU8EUN".to_string(),
        "PA3EEP".to_string(),
        "DL2HAH".to_string(),
        "M7RWA".to_string(),
        "2E0IKV".to_string(),
        "G4HSB".to_string(),
        "G4GOC".to_string(),
        "G8MCD".to_string(),
        "DL0PBS".to_string(),
        "HB9DSB".to_string(),

        "PC8E".to_string(),
        "PE1PMD".to_string(),
    ]
    .into_iter()
    .collect()));

    let now = std::time::Instant::now();
    let spec2 = Spectrum::new(config, &signal);
    let cs = spec2.candidates();
    let t_search = now.elapsed();

    println!("START DECODING");

    let candidates: Vec<Match> = cs.spectrum
        .iter()
        .skip_while(|c| c.loc.freq < args.fmin.unwrap_or(0.0))
        .take_while(|c| c.loc.freq < args.fmax.unwrap_or(f64::INFINITY))
        .filter(|c| c.lrr.ratio() < 4.)
        .map_windows(|&[a,b,c]| {
            let is_max = a.snr2.signal < b.snr2.signal && b.snr2.signal > c.snr2.signal;
            Some(*b).filter(|_| true || is_max || args.verbose)
        })
        .flatten()
        .collect();

    let candidates = filter_candidates(&candidates);
    let mut candidates: Vec<_> = candidates.par_iter().map(|loc| {
        let sym = spec2.extract(loc);
        (loc, sym)
    }).filter(|(_,sym)| { args.verbose || sym.snr_sync_coh_arc::<16>().db() > 1.7 || sym.snr_sync_coh::<16>().db() > 1.7})
    .map(|(loc, sym)| {
        let dat = sym.data().unshuffle().diff();
        let dec = ChiStar::search(&dat, 22);
        (loc, sym, dat, dec)
    }).collect();

    candidates.sort_by(|a,b|a.0.loc.freq.total_cmp(&b.0.loc.freq));
    
    for (loc, sym, dat, dec) in &candidates {
        print!(
            "{} | {:+6.2} {:+6.2} {:+6.2} {:+6.2} {:+6.2}  | {:5.1}% {:5.1}s {:10} {:3} | ",
            loc,

            sym.snr_sync().db(),
            sym.snr_sync_coh::<1>().db(),
            sym.snr_sync_coh::<4>().db(),
            sym.snr_sync_coh::<8>().db(),
            sym.snr_sync_coh::<16>().db(),

            dec.likelyhood * 100.0,
            dec.duration.as_secs_f64(),
            dec.iterations,
            dec.shrinks,
        );


        if let Some(msg) = Message::decode(dec.data) {
            {missing.lock().unwrap().remove(&msg.callsign.to_string());};
            println!(" | {}", list.color(msg.callsign, msg.locator, msg.power));
            plot_signal(0, **loc, &sym, &dec).unwrap();

            // REFINE
            // let l2 = Location2 {
            //     time: c.time,
            //     freq: c.freq + 1.5 * SYMBOL_DISTANCE,
            //     drift: 0.0
            // };
            // let res = refine(&spec2.signal, &l2, Some(dec.data));
            // plot_signal(1, c, &res.symbols, &dec).unwrap();
            // println!();
            // println!(" | {:9.8} {:9.8} {:9.6} | {:?}", res.location.freq, res.location.drift, res.coherence, res.ij);
            // println!("{:50b}", dec.data);

            // let data2 = res.symbols.data().unshuffle().diff();
            // let dec2 = ChiStar::search(&data2, 22);
            // if let Some(msg) = Message::decode(dec2.data) {
            //     println!("{} {} {} {} {}", msg.callsign, msg.locator, msg.power, dec2.likelyhood, dec2.iterations);
            // }
        }

        println!("{:67} {:+6.2} {:+6.2} {:+6.2} {:+6.2}", "",
            sym.snr_sync_coh_arc::<1>().db(),
            sym.snr_sync_coh_arc::<4>().db(),
            sym.snr_sync_coh_arc::<8>().db(),
            sym.snr_sync_coh_arc::<16>().db());
    }

    println!();
    println!("Missing:        {:?}", missing.lock().unwrap().clone());
    println!("Time searching: {:?}", t_search);

    Ok(())
}

fn filter_candidates(candidates: &Vec<Match>) -> Vec<Match> {
    let mut candidates = candidates.clone();
    candidates
}
