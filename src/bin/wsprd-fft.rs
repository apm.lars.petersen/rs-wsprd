use num_complex::Complex64;
use rustfft::num_traits::Zero;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut plan = rustfft::FftPlanner::<f64>::new();
    let fftf = plan.plan_fft_forward(256);
    let ffti = plan.plan_fft_inverse(256);

    let mut buf = [Complex64::zero(); 256];
    buf[1] = Complex64::new(1.0, 0.0);
    buf[2] = Complex64::new(1.0, 0.0);

    for i in 0..3 {
        println!("--- {}", i);

        ffti.process(&mut buf);

        for b in buf.iter().take(10) {
            println!("{}", b);
        }

        println!();

        fftf.process(&mut buf);

        for b in buf.iter().take(10) {
            println!("{}", b);
        }
    }

    Ok(())
}
