use std::thread::sleep;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut qdx = phenol::qdx::Qdx::new()?;
    qdx.consume(Box::new(phenol::recording::WisprChunker::new()));
    sleep(std::time::Duration::from_secs(u64::MAX));
    Ok(())
}
