use num_complex::Complex64;
use rustfft::{algorithm::Radix4, num_traits::Zero, Fft, FftDirection};
use crate::{constants::{SYMBOL_COUNT, SYMBOL_DISTANCE, SYNC}, location::Location, rate::Rate, signal::Signal, snr::{self, SNR}, symbols::Symbols};


#[derive(Debug, Clone, Copy, Default)]
pub struct RefineResult {
    pub location: Location,
    pub snr: SNR,
    pub symbols: Symbols<[Complex64; 4]>,
    pub ij: (i32, i32),
}

pub fn refine(signal: &Signal, loc: &Location, data: Option<u64>) -> RefineResult {
    const RATE: f64 = Rate::Hz0k375.hz() as f64;
    const BLK_LEN: usize = 256;

    const F_CENTER: f64 = 1500.0;

    const INTVL_F: f64 = 0.001;
    const INTVL_D: f64 = 0.0001;

    assert_eq!(signal.rate(), Rate::Hz0k375);

    let offset = (loc.time * RATE) as usize;
    let signal = signal.slice(offset..offset + SYMBOL_COUNT * BLK_LEN);

    let mut res = RefineResult::default();
    let mut buf = [Complex64::zero(); BLK_LEN];
    let mut sym = Symbols::<[Complex64; 4]>::new();
    let fft = Radix4::<f64>::new(BLK_LEN, FftDirection::Forward);

    let drift = loc.drift;
    let shift = F_CENTER - loc.freq + 1.5 * SYMBOL_DISTANCE;

    let data = data.map(|d| Symbols::<u8>::encode(d));
    let mut y = Symbols::<Complex64>::new();
    let mut z = Symbols::<(Complex64,f64)>::new();

    let mut sig = signal.clone();

    let w = 256;
    let v = 256;

    for si in -512..=-254 {
        for di in -w..=w {
            let s = si as f64 * INTVL_F;
            let d = di as f64 * INTVL_D;

            let ldrift = drift + d;
            let lshift = shift + s;
            let cshift = lshift - ldrift * (256.0 * 81.0) / 375.0;

            sig.copy_from_slice(&signal);
            sig.shift_and_drift_mut(cshift, ldrift);

            for i in 0..SYMBOL_COUNT {
                let blk = &sig[i * BLK_LEN..][..BLK_LEN];
                buf.copy_from_slice(blk);
                fft.process(&mut buf);
                sym[i][0] = buf[0];
                sym[i][1] = buf[1];
                sym[i][2] = buf[2];
                sym[i][3] = buf[3];
                y[i] = if let Some(dat) = data {
                    sym[i][dat[i] as usize]
                } else {
                    if SYNC[i] == 0 { sym[i][0] + sym[i][2] } else { sym[i][1] + sym[i][2] }
                };
                z[i] = if let Some(dat) = data {
                    match dat[i] {
                        0 => (sym[i][0], sym[i][1].norm_sqr() + sym[i][2].norm_sqr() + sym[i][3].norm_sqr()),
                        1 => (sym[i][1], sym[i][0].norm_sqr() + sym[i][2].norm_sqr() + sym[i][3].norm_sqr()),
                        2 => (sym[i][2], sym[i][0].norm_sqr() + sym[i][1].norm_sqr() + sym[i][3].norm_sqr()),
                        _ => (sym[i][3], sym[i][0].norm_sqr() + sym[i][1].norm_sqr() + sym[i][2].norm_sqr()),
                    }
                } else {
                    panic!()
                    // match SYNC[i] {
                    //     0 => (sym[i][0] + sym[i][2], sym[i][1].norm_sqr() + sym[i][3].norm_sqr()),
                    //     _ => (sym[i][1] + sym[i][3], sym[i][0].norm_sqr() + sym[i][2].norm_sqr()),
                    // }
                };
            }

            //let coherence = coherence(&y);
            let snr = coherence_snr(&z);

            //if coherence > res.coherence {
            if snr > res.snr {
                //res.coherence = coherence;
                res.snr = snr;
                res.location.time = loc.time;
                res.location.freq = F_CENTER - lshift + 1.5 * SYMBOL_DISTANCE;
                res.location.drift = ldrift;
                res.symbols = sym.clone();
                res.ij = (si, di);
            }
        }
    }

    res
}

fn coherence(y: &Symbols<Complex64>) -> f64 {
    let mut c = 0.0;
    y.array_windows::<16>().for_each(|win| {
        c += win.iter().sum::<Complex64>().norm_sqr();
    });
    c
}

pub fn coherence_snr(y: &Symbols<(Complex64,f64)>) -> SNR {
    let mut snr = snr::SNR::zero();
    y.array_windows::<16>().for_each(|win| {
        let cl = win.iter().fold((Complex64::zero(), 0.0), |acc, (c, n)| {
            (acc.0 + c, acc.1 + n)
        });
        snr.signal += cl.0.norm_sqr();
        snr.noise += cl.1;
    });
    snr
}
