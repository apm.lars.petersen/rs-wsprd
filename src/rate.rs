use clap::{builder::PossibleValue, ValueEnum};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
pub enum Rate {
    Hz0k375,
    Hz0k75,
    Hz1k5,
    Hz3k,
    Hz6k,
    Hz12k,
    Hz24k,
    Hz48k,
}

impl Rate {
    pub fn new(hz: usize) -> Result<Self, Box<dyn std::error::Error>> {
        match hz {
            48_000 => Ok(Self::Hz48k),
            24_000 => Ok(Self::Hz24k),
            12_000 => Ok(Self::Hz12k),
            6_000 => Ok(Self::Hz6k),
            3_000 => Ok(Self::Hz3k),
            1_500 => Ok(Self::Hz1k5),
            750 => Ok(Self::Hz0k75),
            375 => Ok(Self::Hz0k375),
            _ => Err("invalid sample rate".into())
        }
    }

    pub const fn hz(&self) -> usize {
        match self {
            Self::Hz48k => 48_000,
            Self::Hz24k => 24_000,
            Self::Hz12k => 12_000,
            Self::Hz6k => 6_000,
            Self::Hz3k => 3_000,
            Self::Hz1k5 => 1_500,
            Self::Hz0k75 => 750,
            Self::Hz0k375 => 375,
        }
    }

    pub fn block_len(&self) -> usize {
        match self {
            Self::Hz48k => 32_768,
            Self::Hz24k => 16_384,
            Self::Hz12k => 8_192,
            Self::Hz6k => 4_096,
            Self::Hz3k => 2_048,
            Self::Hz1k5 => 1_024,
            Self::Hz0k75 => 512,
            Self::Hz0k375 => 256,
        }
    }
}

impl ValueEnum for Rate {
    fn value_variants<'a>() -> &'a [Self] {
        &[Self::Hz48k, Self::Hz24k, Self::Hz12k, Self::Hz6k, Self::Hz3k, Self::Hz1k5, Self::Hz0k75, Self::Hz0k375 ]
    }

    fn to_possible_value(&self) -> Option<PossibleValue> {
        Some(match self {
            Self::Hz48k => PossibleValue::new("48K"),
            Self::Hz24k => PossibleValue::new("24K"),
            Self::Hz12k => PossibleValue::new("12K"),
            Self::Hz6k => PossibleValue::new("6K"),
            Self::Hz3k => PossibleValue::new("3K"),
            Self::Hz1k5 => PossibleValue::new("1K5"),
            Self::Hz0k75 => PossibleValue::new("0K75"),
            Self::Hz0k375 => PossibleValue::new("0K375"),
        })
    }
}

impl std::fmt::Display for Rate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.to_possible_value()
            .expect("no values are skipped")
            .get_name()
            .fmt(f)
    }
}

impl std::str::FromStr for Rate {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        for variant in Self::value_variants() {
            if variant.to_possible_value().unwrap().matches(s, false) {
                return Ok(*variant);
            }
        }
        Err(format!("invalid variant: {s}"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ord() {
        assert!(Rate::Hz0k375 < Rate::Hz48k)
    }
}
